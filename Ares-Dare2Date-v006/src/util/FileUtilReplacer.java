package util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;
import java.io.FileWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

import javax.swing.JRadioButton;

import view.ChangeSettings;


/*
 * 
 * 
 * */

public class FileUtilReplacer {
	private static String stuff = "";
	private static int changelevel = 0;

	public void removeLineFromFile(String file, String lineToRemove) {
		if (ChangeSettings.isChgeMusic() == true

				) {
					changelevel = 11;
					setStuff(ChangeSettings.getTfNewMusic().getText());
				}
		
		if (ChangeSettings.isChgeHobbies() == true

				) {
					changelevel = 12;
					setStuff(ChangeSettings.getTfNewHobbies().getText());
				}
		
		if (ChangeSettings.isChgeMovies() == true

				) {
					changelevel = 13;
					setStuff(ChangeSettings.getTfNewMovies().getText());
				}if (ChangeSettings.isChgeSport() == true

						) {
							changelevel = 14;
							setStuff(ChangeSettings.getTfNewSport().getText());
						}
		
		
		
		if (ChangeSettings.isChngePwd() == true) {
			changelevel = 3;
			setStuff(ChangeSettings.getTxtNewpwd().getText());
		}

		if (ChangeSettings.isChngeCity() == true) {
			changelevel = 1;
			setStuff(ChangeSettings.getTxtNewcity().getText());
		}

		if (ChangeSettings.isChngeCrdtCdNum() == true) {
			changelevel = 2;
			setStuff(ChangeSettings.getTxtNewcrdtcdnum().getText());
		}
		// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		if (ChangeSettings.isChangeMembertype() == true) {
			changelevel = 5;

			if (ChangeSettings.getRdbtnVip1().isSelected()) {
				stuff = "vip";

			}
			if (ChangeSettings.getRdbtnRegular1().isSelected()) {
				stuff = "regular";

			}

		}

		if (ChangeSettings.isChngeSX() == true) {

			changelevel = 4;

			if (ChangeSettings.getRdbtnMan().isSelected()) {
				stuff = "man";

			}
			if (ChangeSettings.getRdbtnWomen().isSelected()) {
				stuff = "woman";

			}
//			if (ChangeSettings.getRdbtnOther().isSelected()) {
//				stuff = "both";
//
//			}

		}

		// if(){ String stuff=ChangeSettings.txtNewcrdtcdnum.getText();}

		ChangeSettings.setChngePwd(false);
		ChangeSettings.setChngeSX(false);
		ChangeSettings.setChngeCrdtCdNum(false);
		ChangeSettings.setChngeCity(false);
		ChangeSettings.setChangeMembertype(false);

		// String stuff="kkkkkkkkkkkkkkkkkkk";
		try {

			File inFile = new File(file);

			if (!inFile.isFile()) {
				System.out.println("Parameter is not an existing file");
				return;
			}

			// Construct the new file that will later be renamed to the original
			// filename.
			File tempFile = new File(inFile.getAbsolutePath() + ".tmp");

			BufferedReader br = new BufferedReader(new FileReader(file));
			PrintWriter pw = new PrintWriter(new FileWriter(tempFile));

			String line = null;

			// Read from the original file and write to the new
			// unless content matches data to be removed.
			while ((line = br.readLine()) != null) {

				//if (!line.trim().equals(lineToRemove)) {
					if (!line.equals(lineToRemove)) {
					pw.println(line);
					pw.flush();
					// pw.append(stuff);
				}

			//	if ((line.trim().equals(lineToRemove))) {
					if ((line.equals(lineToRemove))) {

					if (changelevel == 1) {
						if (ChangeSettings.getAddressH().equals(lineToRemove)) {

							pw.append(getStuff() + "\n");
							pw.flush();
						}
					}
					if (changelevel == 2) {
						if (ChangeSettings.getCreditCardNumberH().equals(lineToRemove)) {

							pw.append(getStuff() + "\n");
							pw.flush();
						}
					}
					if (changelevel == 3) {
						if (ChangeSettings.getPasswordH().equals(lineToRemove)) {

							pw.append(getStuff() + "\n");
							pw.flush();
						}
					}
					if (changelevel == 4) {
						if (ChangeSettings.getBlankSP().equals(lineToRemove)) {

							pw.append(getStuff() + "\n");
							pw.flush();
						}
					}
					if (changelevel == 5) {
						if (ChangeSettings.getBlankMT().equals(lineToRemove)) {

							pw.append(getStuff() + "\n");
							pw.flush();
						}
					}
					if (changelevel == 11) {
						if (ChangeSettings.getBlankMusic().equals(lineToRemove)) {

							pw.append(getStuff() + "\n");
							pw.flush();
						}
					}	if (changelevel == 12) {
						if (ChangeSettings.getBlankHobbies().equals(lineToRemove)) {

							pw.append(getStuff() + "\n");
							pw.flush();
						}
					}	if (changelevel == 13) {
						if (ChangeSettings.getBlankMovies().equals(lineToRemove)) {

							pw.append(getStuff() + "\n");
							pw.flush();
						}
					}	if (changelevel == 14) {
						if (ChangeSettings.getBlankSports().equals(lineToRemove)) {

							pw.append(getStuff() + "\n");
							pw.flush();
						}
					}
					
					
					
					
					
					
					
					
					
					
					
					
				}
			}
			pw.close();
			br.close();

			// Delete the original file
			if (!inFile.delete()) {
				System.out.println("Could not delete file");
				return;
			}

			// Rename the new file to the filename the original file had.
			if (!tempFile.renameTo(inFile))
				System.out.println("Could not rename file");

		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public static String getStuff() {
		return stuff;
	}

	public void setStuff(String stuff) {
		FileUtilReplacer.stuff = stuff;
	}

	/*
	 * public static void main(String[] args) { FileUtilReplacer util = new
	 * FileUtilReplacer();
	 * 
	 * util.removeLineFromFile("Res/1000.txt", "4000");
	 * System.out.println("=============DONE==============="); }
	 */
}