package util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import view.Inbox;


public class FileUtilDeleter {
	private File file = null;

	/**
	 * Constructor
	 * 
	 * @param file
	 */
	public FileUtilDeleter(File file) {
		this.file = file;

		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Delete entry
	 * 
	 * @param entry
	 * @return true if successfully deleted
	 */
	public boolean deleteEntry(String entry) {
		try {
			int lineCounter = 0;
			// Construct the new file that will later be renamed to the original
			// filename.
			File tempFile = new File(file.getAbsolutePath() + ".tmp");

			BufferedReader br = new BufferedReader(new FileReader(file));
			PrintWriter pw = new PrintWriter(new FileWriter(tempFile));

			// Read from the original file and write to the new
			// unless content matches data to be removed.
			String line;
			while ((line = br.readLine()) != null) {
				lineCounter++;
				if (!line.equals(entry)) {
					pw.println(line);
					pw.flush();
				}
				if (line.equals(entry)) {

					// line 15 annalysis

					if ((lineCounter <= 14)||((lineCounter > 14)
							&& (((lineCounter == 15) && (Inbox.isFirstMsgDel() == false))
							||((lineCounter == 16) )
						//	||((lineCounter == 15) && (Inbox.isFirstMsgDel() == true)&&(Inbox.isFirstroundOfdelete()== false))
							|| ((lineCounter == 17) && (Inbox.isSecondMsgDel() == false))
							||((lineCounter == 18) )
						//	|| ((lineCounter == 17) && (Inbox.isSecondMsgDel() == true)&&(Inbox.isFirstroundOfdelete()== false))
							|| ((lineCounter == 19) && (Inbox.isThirdMsgDel() == false))
							|| ((lineCounter == 20) )
							//|| ((lineCounter == 19) && (Inbox.isThirdMsgDel() == true)&&(Inbox.isFirstroundOfdelete()== false))
							|| ((lineCounter == 21) && (Inbox.isFourthMsgDel() == false))
							|| ((lineCounter == 22))
							//|| ((lineCounter == 21) && (Inbox.isFourthMsgDel() == true)&&(Inbox.isFirstroundOfdelete()== false))
							|| ((lineCounter == 23) && (Inbox.isFifthMsgDel() == false))
						//	|| ((lineCounter == 24))
							//|| ((lineCounter == 23) && (Inbox.isFifthMsgDel() == true)&&(Inbox.isFirstroundOfdelete()== false))
							||((lineCounter >= 24)
									//&&(Inbox.isFirstroundOfdelete()== true)
									)
					//		||((lineCounter >= 23)&&(Inbox.isFirstroundOfdelete()== false))
							))) {
						pw.println(line);
						pw.flush();

					}

					if ((lineCounter == 15)
							&& (Inbox.isFirstMsgDel() == true)) {
						// skip
						System.out
						.println("********************Deleted:message1 ************"
								+ entry);

					}

					if ((lineCounter == 17)
							&& (Inbox.isSecondMsgDel() == true)) {
						// skip
						System.out
						.println("********************Deleted:message2 ************"
								+ entry);

					}
					if ((lineCounter == 19)
							&& (Inbox.isThirdMsgDel() == true)) {
						// skip

						System.out
						.println("********************Deleted:message3************"
								+ entry);
					}
					if ((lineCounter == 21)
							&& (Inbox.isFourthMsgDel() == true)) {
						// skip
						System.out
						.println("********************Deleted:message4 ************"
								+ entry);

					}
					if ((lineCounter == 23)
							&& (Inbox.isFifthMsgDel() == true)) {
						// skip
						System.out
						.println("********************Deleted:message5 ************"
								+ entry);

					}

				}

			}
			pw.close();
			br.close();

			// Delete the original file
			if (!file.delete()) {
				System.out.println("Could not delete file");
				return false;
			}

			// Rename the new file to the filename the original file had.
			if (!tempFile.renameTo(file)) {
				System.out.println("Could not rename file");
				return false;
			}

			return true;
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * @return the file
	 */
	public File getFile() {
		return file;
	}

	/**
	 * @param file
	 *            the file to set
	 */
	public void setFile(File file) {
		this.file = file;
	}
	// tester main runner
	/*
	 * public static void main(String[] args) { FileUtilDeleter mod = new
	 * FileUtilDeleter(new File("Text-Files/250.txt"));
	 * 
	 * mod.deleteEntry("nkomonl");
	 * System.out.println("*******************************");
	 * 
	 * }
	 */
}