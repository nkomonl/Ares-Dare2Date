package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import util.VIP_Updater;
import model.ReadTxt4Login;
import model.SendMessage;
import javax.swing.SwingConstants;

public class SendMessagePane extends JPanel{
	
	private static JTextField txtAddReceiver;
	private JTextArea txtrPrivatemessage;
	
	private static String addReceiver = "Add receiver";
	private static String message;
	private static String receiverID;
	private static String sendrID = ReadTxt4Login.getLine1();
	private static String privateMessage = "Private Message";
	private static String blankSender2;
	private static JComboBox<String> listScrollPane;
	
	public SendMessagePane(){

		Panel panel = new Panel();
		panel.setBounds(489, 116, 105, -101);
		add(panel);

		txtAddReceiver = new JTextField();
		txtAddReceiver.setText(getAddReceiver());
		txtAddReceiver.setFont(new Font("Yu Gothic Light", Font.PLAIN, 12));
		txtAddReceiver.setForeground(new Color(128, 0, 0));
		txtAddReceiver.setBounds(51, 33, 620, 20);
		add(txtAddReceiver);
		txtAddReceiver.setColumns(10);

		// /*********************************+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		JButton btnSend = new JButton("Send Private Message");
		btnSend.setBounds(269, 457, 399, 20);
		btnSend.setForeground(new Color(128, 0, 0));
		btnSend.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		add(btnSend);

		btnSend.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				SendMessage.setEmailing(true);
				System.out.print("\n**send clikked......\n");

				setReceiverID((txtAddReceiver.getText()));

				if (receiverID.equals("Add receiver")) {

					JOptionPane.showMessageDialog(null,
							"Must select message recipient from the registered users! "
									+ getReceiverID(), "Select Receiver",
							JOptionPane.INFORMATION_MESSAGE);

				}

				if (!receiverID.equals("Add receiver")) {
					String strippedOfTabs = txtrPrivatemessage.getText().replaceAll("\\s+", " ");
					System.out.println("\nkkkkkkkkkkkkkkkkkkkkkkk"+ strippedOfTabs + "kkkkkkkkkkkkkkkk\n ");
					setMessage(strippedOfTabs);
					 SendMessage.writeNewMessage();
					System.out.println("the reciver is .." + getReceiverID()
							+ "...the message is \n" + getMessage());
					JOptionPane.showMessageDialog(null, "Message sent to "
							+ getReceiverID(), "Successfully Sent",
							JOptionPane.INFORMATION_MESSAGE);

//					MainFrame.setScreenNumber(4);
//					panelManager.panelManagerI();
				}
			}
		});

		Panel panel_2 = new Panel();
		panel_2.setForeground(new Color(255, 255, 255));
		panel_2.setFont(new Font("Yu Gothic Light", Font.PLAIN, 14));
		panel_2.setBackground(new Color(128, 0, 0));
		panel_2.setBounds(51, 81, 198, 396);
		add(panel_2);
		panel_2.setLayout(null);

		JLabel lblDaredate = new JLabel("Dare2Date Registered Users");
		lblDaredate.setHorizontalAlignment(SwingConstants.CENTER);
		lblDaredate.setBounds(0, 3, 197, 22);
		lblDaredate.setFont(new Font("Yu Gothic Light", Font.BOLD, 13));
		lblDaredate.setForeground(new Color(255, 255, 255));
		panel_2.add(lblDaredate);

		// action listner for thescroll list
	listScrollPane = new JComboBox<String>();
		listScrollPane.setBounds(10, 36, 178, 30);
		listScrollPane.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String selectedItem = (String) listScrollPane.getSelectedItem();
				
				txtAddReceiver.setText(selectedItem);

			}
		});
		
		
		
	
		
		panel_2.add(listScrollPane);

		// create list
		// usersList = new JList<>(listModel1);
		// usersList.setBounds(63, 270, 98, 110);
		// panel.add(new JScrollPane(usersList));
		// panel.add(usersList);
		//
		//

		txtrPrivatemessage = new JTextArea();
		txtrPrivatemessage.setText("Write your message here!\r\n");
		txtrPrivatemessage.setForeground(new Color(255, 255, 255));
		txtrPrivatemessage.setFont(new Font("Yu Gothic Light", Font.PLAIN, 14));
		txtrPrivatemessage.setBackground(new Color(128, 0, 0));
		txtrPrivatemessage.setBounds(269, 81, 399, 375);
		add(txtrPrivatemessage);
		
		loadmodel();
		
	}

	public void loadmodel() {
		listScrollPane.removeAllItems();
		for (int yu = 0; yu <VIP_Updater.getArrayListALLUsers().size(); yu++) {
			listScrollPane.addItem(VIP_Updater.getArrayListALLUsers().get(yu));
		}
				
	}
	public static JTextField getTxtAddReceiver() {
		return txtAddReceiver;
	}

	public void setTxtAddReceiver(JTextField txtAddReceiver) {
		this.txtAddReceiver = txtAddReceiver;
	}

	public static String getAddReceiver() {
		return addReceiver;
	}

	public static void setAddReceiver(String addReceiver) {
		SendMessagePane.addReceiver = addReceiver;
	}

	public static String getMessage() {
		return message;
	}

	public static void setMessage(String message) {
		SendMessagePane.message = message;
	}

	public static String getReceiverID() {
		return receiverID;
	}

	public static void setReceiverID(String receiverID) {
		SendMessagePane.receiverID = receiverID;
	}

	public static String getSendrID() {
		return sendrID;
	}

	public static void setSendrID(String sendrID) {
		SendMessagePane.sendrID = sendrID;
	}

	public static String getPrivateMessage() {
		return privateMessage;
	}

	public static void setPrivateMessage(String privateMessage) {
		SendMessagePane.privateMessage = privateMessage;
	}

	public static String getBlankSender2() {
		return blankSender2;
	}

	public static void setBlankSender2(String blankSender2) {
		SendMessagePane.blankSender2 = blankSender2;
	}
	
	

}
