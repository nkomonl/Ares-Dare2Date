package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.UIManager;


import model.SendMessage;
//import model.SendMessage;
import model.WriteSignUp;

import javax.swing.JCheckBox;

import util.PanelManager;
//import util.VIP_Updater;
import controller.MainFrame;

public class SignUpScreen_Panel2 extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField txtWeWontDo;
	private static JTextField tfname;
	private static JTextField tfsname;
	private static JTextField Adrstfield;
	private static JTextField tfcreditCard;
	private static JPasswordField pwdField;
	private static String genderx = "";
	private static String genderxLiked = "";
	private static String VIPstatus = "";
	private static String music,music1,music2,music3,music4,music5,music6,music7,music8="";
	private static String movieTypes,movieTypes1,movieTypes2,movieTypes3,movieTypes4,movieTypes5,movieTypes6="";
	private static String hobbies;
	private static String sports,sports1,sports2,sports3,sports4,sports5,sports6,sports7="";
	private JTextField txtEmail;
	private JTextField txtzipcode;
	private JTextField City;
	private JTextField tfOtherSports;
	private JTextField tfmusic;
	private static JTextField textField_4;
	private JTextField textField;
	private JTextField tfcountry;
	private JTextField textField_1;
	private JTextField textField_2;
	private static boolean autoNewuserupdater=false;


	public SignUpScreen_Panel2(final PanelManager panelManager) {
		setBackground(UIManager.getColor("CheckBox.background"));

		// setBackground(Color.MAGENTA);
		setLayout(null);
		setBounds(350, 20, 800, 700);
		setBackground(Color.WHITE);

		JLabel lblNewLabel = new JLabel("Header");
		lblNewLabel.setIcon(new ImageIcon("pic/loginscreenlogo.png"));
		lblNewLabel.setBounds(0, 0, 871, 100);
		add(lblNewLabel);
				
		setAutoNewuserupdater(false);
		
		
		//Textfields!
		tfname = new JTextField();
		tfname.setBounds(169, 111, 137, 20);
		add(tfname);
		tfname.setColumns(10);

		tfsname = new JTextField();
		tfsname.setBounds(169, 136, 137, 20);
		add(tfsname);
		tfsname.setColumns(10);


		Adrstfield = new JTextField();
		Adrstfield.setBounds(423, 218, 137, 20);
		add(Adrstfield);
		Adrstfield.setColumns(10);
		/*
		setTfcreditCard(new JTextField());
		getTfcreditCard().setBounds(569, 58, 137, 20);
		add(getTfcreditCard());
		getTfcreditCard().setColumns(10);
		*/
		//J labels!
		JLabel lblName = new JLabel("Name:");
		lblName.setForeground(new Color(128, 0, 0));
		lblName.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		lblName.setBounds(62, 114, 95, 14);
		add(lblName);
		
		JLabel lblSurname = new JLabel("Surname:");
		lblSurname.setForeground(new Color(128, 0, 0));
		lblSurname.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		lblSurname.setBounds(62, 139, 95, 14);
		add(lblSurname);
		
		JLabel lblZipCode = new JLabel("Zip code:");
		lblZipCode.setForeground(new Color(128, 0, 0));
		lblZipCode.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		lblZipCode.setBounds(62, 221, 95, 14);
		add(lblZipCode);
		
		
		JLabel lblAddress = new JLabel("City:");
		lblAddress.setForeground(new Color(128, 0, 0));
		lblAddress.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		lblAddress.setBounds(342, 221, 75, 14);
		add(lblAddress);
		
		JLabel lblCountry= new JLabel("Country:");
		lblCountry.setForeground(new Color(128, 0, 0));
		lblCountry.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		lblCountry.setBounds(575, 221, 75, 14);
		add(lblCountry);
		
		JLabel lblSex = new JLabel("Sex.Preferences:");
		lblSex.setForeground(new Color(128, 0, 0));
		lblSex.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		lblSex.setBounds(62, 328, 154, 14);
		add(lblSex);


		JLabel lblMembership = new JLabel("Membership :");
		lblMembership.setForeground(new Color(128, 0, 0));
		lblMembership.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		lblMembership.setBounds(62, 277, 95, 14);
		add(lblMembership);

		JLabel lblNumber = new JLabel("Number:");
		lblNumber.setForeground(new Color(128, 0, 0));
		lblNumber.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		lblNumber.setBounds(342, 196, 75, 14);
		add(lblNumber);
		
		JLabel lblCreditcardnumber = new JLabel("CreditCard:");
		lblCreditcardnumber.setForeground(new Color(128, 0, 0));
		lblCreditcardnumber.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		lblCreditcardnumber.setBounds(342, 277, 75, 14);
		add(lblCreditcardnumber);
		

		JLabel lblGender = new JLabel("Gender :");
		lblGender.setForeground(new Color(128, 0, 0));
		lblGender.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		lblGender.setBounds(62, 246, 95, 14);
		add(lblGender);
		
		JLabel lblYourPersonalInformation = new JLabel(	"Preferences (Other users might see this information)");
		lblYourPersonalInformation.setHorizontalAlignment(SwingConstants.CENTER);
		lblYourPersonalInformation.setForeground(new Color(128, 0, 0));
		lblYourPersonalInformation.setFont(new Font("Yu Gothic Light", Font.BOLD | Font.ITALIC, 11));
		lblYourPersonalInformation.setBounds(241, 362, 344, 14);
		add(lblYourPersonalInformation);
		
		
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setForeground(new Color(128, 0, 0));
		lblPassword.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		lblPassword.setBounds(62, 302, 70, 15);
		add(lblPassword);

		pwdField = new JPasswordField();
		pwdField.setBounds(169, 298, 137, 19);
		add(pwdField);
		
		JLabel lblMovieChoices = new JLabel("Movies:");
		lblMovieChoices.setForeground(new Color(128, 0, 0));
		lblMovieChoices.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		lblMovieChoices.setBounds(62, 388, 97, 14); 
		add(lblMovieChoices);
		
		JLabel lblMusicGenre = new JLabel("Music:");
		lblMusicGenre.setForeground(new Color(128, 0, 0));
		lblMusicGenre.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		lblMusicGenre.setBounds(62, 470, 97, 14);
		add(lblMusicGenre);
		
		JLabel lblSports = new JLabel("Sports:");
		lblSports.setForeground(new Color(128, 0, 0));
		lblSports.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		lblSports.setBounds(62, 413, 46, 14);
		add(lblSports);
		
		JLabel lblHobbies = new JLabel("Hobbies:");
		lblHobbies.setForeground(new Color(128, 0, 0));
		lblHobbies.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		lblHobbies.setBounds(62, 553, 61, 14);
		add(lblHobbies);

		JLabel lblYearOfBirth = new JLabel("Birth date:");
		lblYearOfBirth.setForeground(new Color(128, 0, 0));
		lblYearOfBirth.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		lblYearOfBirth.setBounds(62, 167, 95, 14);
		add(lblYearOfBirth);
		

		textField_4 = new JTextField("");//birthday
		textField_4.setBounds(352, 164, 95, 20);
		add(textField_4);
		textField_4.setColumns(4);
		
		
		JLabel lblEmail = new JLabel("E-mail:");
		lblEmail.setForeground(new Color(128, 0, 0));
		lblEmail.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		lblEmail.setBounds(342, 145, 70, 14);
		add(lblEmail);
		
		
		JLabel lblOtherSportChoices = new JLabel("Other, namely:");
		lblOtherSportChoices.setForeground(new Color(128, 0, 0));
		lblOtherSportChoices.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		lblOtherSportChoices.setBounds(182, 521, 154, 14);
		add(lblOtherSportChoices);
		
		JLabel lblAddress_1 = new JLabel("Address:");
		lblAddress_1.setForeground(new Color(128, 0, 0));
		lblAddress_1.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		lblAddress_1.setBounds(62, 196, 70, 14);
		add(lblAddress_1);
		
		//RadioButtons:
		
		JRadioButton rdbtnMan = new JRadioButton("Man");
		rdbtnMan.setBackground(Color.WHITE);
		rdbtnMan.setBounds(180, 242, 109, 23);
		add(rdbtnMan);

		
		JRadioButton rdbtnWomen = new JRadioButton("Woman");
		rdbtnWomen.setBackground(Color.WHITE);
		rdbtnWomen.setBounds(291, 242, 109, 23);
		add(rdbtnWomen);

		JRadioButton rdbtnOther = new JRadioButton("Other");
		rdbtnOther.setBackground(Color.WHITE);
		rdbtnOther.setBounds(415, 242, 109, 23);
		add(rdbtnOther);
		
		
		ButtonGroup gender = new ButtonGroup();
		gender.add(rdbtnMan);
		gender.add(rdbtnWomen);
		gender.add(rdbtnOther);
		
		
		JRadioButton rdbtnM = new JRadioButton("Male");
		rdbtnM.setBackground(Color.WHITE);
		rdbtnM.setBounds(182, 324, 109, 23);
		add(rdbtnM);
		
		JRadioButton rdbtnF = new JRadioButton("Female");
		rdbtnF.setBackground(Color.WHITE);
		rdbtnF.setBounds(291, 324, 109, 23);
		add(rdbtnF);

		JRadioButton rdbtnBoth = new JRadioButton("Both");
		rdbtnBoth.setBackground(Color.WHITE);
		rdbtnBoth.setBounds(415, 324, 95, 23);
		add(rdbtnBoth);

		JRadioButton rdbtnOther_1 = new JRadioButton("Other");
		rdbtnOther_1.setBackground(Color.WHITE);
		rdbtnOther_1.setBounds(514, 324, 109, 23);
		add(rdbtnOther_1);
		
		ButtonGroup sexualPreference = new ButtonGroup();
		sexualPreference.add(rdbtnM);
		sexualPreference.add(rdbtnF);
		sexualPreference.add(rdbtnBoth);
		sexualPreference.add(rdbtnOther_1);
		
		
		JRadioButton rdbtnVip = new JRadioButton("VIP");
		rdbtnVip.setBackground(Color.WHITE);
		rdbtnVip.setBounds(291, 273, 46, 23);
		add(rdbtnVip);

		JRadioButton rdbtnRegular = new JRadioButton("Regular");
		rdbtnRegular.setBackground(Color.WHITE);
		rdbtnRegular.setBounds(182, 273, 109, 23);
		add(rdbtnRegular);

		ButtonGroup VIP_reguler = new ButtonGroup();
		VIP_reguler.add(rdbtnVip);
		VIP_reguler.add(rdbtnRegular);

		
		//CheckBoxes
	
		JCheckBox chckbxSoul = new JCheckBox("Soul");
		chckbxSoul.setBackground(Color.WHITE);
		chckbxSoul.setBounds(182, 491, 97, 23);
		add(chckbxSoul);
		
		JCheckBox chckbxSjams = new JCheckBox("Slow jam");
		chckbxSjams.setBackground(Color.WHITE);
		chckbxSjams.setBounds(415, 492, 97, 23);
		add(chckbxSjams);
		
		JCheckBox chckbxJazz = new JCheckBox("Jazz");
		chckbxJazz.setBackground(Color.WHITE);
		chckbxSjams.setBackground(Color.WHITE);
		chckbxJazz.setBounds(291, 492, 97, 23);
		add(chckbxJazz);
		
		JCheckBox chckbxPop = new JCheckBox("Pop");
		chckbxPop.setBackground(Color.WHITE);
		chckbxPop.setBounds(514, 466, 97, 23);
		add(chckbxPop);
		
		JCheckBox chckbxRock = new JCheckBox("Rock");
		chckbxRock.setBackground(Color.WHITE);
		chckbxRock.setBounds(291, 466, 97, 23);
		add(chckbxRock);
		
		JCheckBox chckbxRb = new JCheckBox("R&B");
		chckbxRb.setBackground(Color.WHITE);
		chckbxRb.setBounds(182, 465, 97, 23);
		add(chckbxRb);
				
		JCheckBox chckbxpunk = new JCheckBox("Punk");
		chckbxpunk.setBackground(Color.WHITE);
		chckbxpunk.setBounds(621, 466, 97, 23);
		add(chckbxpunk);
		
		JCheckBox chckbxClassic = new JCheckBox("Classic");
		chckbxClassic.setBackground(Color.WHITE);
		chckbxClassic.setBounds(415, 466, 97, 23);
		add(chckbxClassic);
		
		JCheckBox chckbxAction = new JCheckBox("Action");
		chckbxAction.setBackground(Color.WHITE);
		chckbxAction.setBounds(415, 387, 97, 23);
		add(chckbxAction);
		
		JCheckBox chckbxdDrama = new JCheckBox("Drama");
		chckbxdDrama.setBackground(Color.WHITE);
		chckbxdDrama.setBounds(182, 383, 97, 23);
		add(chckbxdDrama);
		
		JCheckBox chckbxComedy = new JCheckBox("Comedy");
		chckbxComedy.setBackground(Color.WHITE);
		chckbxComedy.setBounds(705, 384, 71, 23);
		add(chckbxComedy);
		
		JCheckBox chckbxHorror = new JCheckBox("Horror");
		chckbxHorror.setBackground(Color.WHITE);
		chckbxHorror.setBounds(514, 383, 97, 23);
		add(chckbxHorror);
		
		JCheckBox chckbxRomance = new JCheckBox("Romance");
		chckbxRomance.setBackground(Color.WHITE);
		chckbxRomance.setBounds(291, 383, 97, 23);
		add(chckbxRomance);
		
		JCheckBox chckbxThriller = new JCheckBox("Thriller");
		chckbxThriller.setBackground(Color.WHITE);
		chckbxThriller.setBounds(621, 383, 97, 23);
		add(chckbxThriller);
		
		
		JCheckBox chckbxSoccer = new JCheckBox("Soccer");
		chckbxSoccer.setBackground(Color.WHITE);
		chckbxSoccer.setBounds(182, 409, 97, 23);
		add(chckbxSoccer);
		
		JCheckBox chkbxRidinghorses = new JCheckBox("riding horses");
		chkbxRidinghorses.setBackground(Color.WHITE);
		chkbxRidinghorses.setBounds(621, 413, 137, 23);
		add(chkbxRidinghorses);
		
		JCheckBox chckbxBasketball = new JCheckBox("basketball");
		chckbxBasketball.setBackground(Color.WHITE);
		chckbxBasketball.setBounds(291, 409, 97, 23);
		add(chckbxBasketball);
		
		JCheckBox chckbxVolleyball = new JCheckBox("volleyball");
		chckbxVolleyball.setBackground(Color.WHITE);
		chckbxVolleyball.setBounds(514, 413, 97, 23);
		add(chckbxVolleyball);
		
		JCheckBox chckbxdance = new JCheckBox("dance");
		chckbxdance.setBackground(Color.WHITE);
		chckbxdance.setBounds(415, 413, 97, 23);
		add(chckbxdance);
		
	
		JButton btnSignUp = new JButton("Sign Up");
		btnSignUp.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		btnSignUp.setBounds(316, 634, 89, 23);
		add(btnSignUp);
		btnSignUp.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.print("Signup clikked......");
				if ((pwdField.getPassword().equals("")) || (getTfcreditCard().getText().equals(""))
						|| (Adrstfield.getText().equals(""))
						|| ((!rdbtnMan.isSelected()) && (!rdbtnWomen.isSelected())) || (tfsname.getText().equals(""))
						|| ((!rdbtnVip.isSelected()) && (!rdbtnRegular.isSelected()))

						|| (tfname.getText().equals("")))

				{
					JOptionPane.showMessageDialog(null, "Please complete the empty fields", "Error",
							JOptionPane.INFORMATION_MESSAGE);

				}

				if ((!pwdField.getPassword().equals("")) && (!getTfcreditCard().getText().equals(""))
						&& (!Adrstfield.getText().equals("")) && (!tfsname.getText().equals(""))
						&& (!tfname.getText().equals(""))
						&& ((rdbtnM.isSelected()) || (rdbtnF.isSelected()) || (rdbtnBoth.isSelected()))
						&& ((rdbtnVip.isSelected()) || (rdbtnRegular.isSelected()))
						&& ((rdbtnMan.isSelected()) || (rdbtnWomen.isSelected())))

				{

					ButtonGroup VIP_reguler = new ButtonGroup();
					VIP_reguler.add(rdbtnVip);
					VIP_reguler.add(rdbtnRegular);

					if (rdbtnVip.isSelected()) {
						VIPstatus = "vip";
						System.out.println("\n\n\nVIP-status is .." + VIPstatus);
					}
					if (rdbtnRegular.isSelected()) {
						VIPstatus = "regular";
						System.out.println("\n\n\nVip-status is .." + VIPstatus);
					}

					if (rdbtnM.isSelected()) {
						genderxLiked = "man";
						System.out.println("\n\n\nGenderLiked is .." + genderxLiked);
					}
					if (rdbtnF.isSelected()) {
						genderxLiked = "woman";
						System.out.println("\n\n\nGenderLiked is .." + genderxLiked);
					}
					if (rdbtnBoth.isSelected()) {
						genderxLiked = "both";
						System.out.println("\n\n\nGenderLiked is .." + genderxLiked);
					}

					if (rdbtnMan.isSelected()) {
						genderx = "man";
						System.out.println("\n\n\nGender is .." + genderx);
					}
					if (rdbtnWomen.isSelected()) {
						genderx = "woman";
						System.out.println("\n\n\nGender is .." + genderx);
					}
					
					//****************************************************************************************collect checkbox*********************************
					//music
					if(chckbxClassic.isSelected()){music1="classic";}
					if(chckbxpunk.isSelected()){music2="punk";}
					if(chckbxRb.isSelected()){music3="r&b";}
					if( chckbxRock .isSelected()){music4="rock";}
					if(chckbxPop.isSelected()){music5="pop";}
					if(chckbxJazz.isSelected()){music6="jazz";}
					if(chckbxSjams.isSelected()){music7="slow jams";}
					
					//movies code
					if(chckbxAction.isSelected()){movieTypes1="action";}
					if(chckbxdDrama.isSelected()){movieTypes2="drama";}
					if(chckbxComedy.isSelected()){movieTypes3="comedy";}
					if(chckbxHorror.isSelected()){movieTypes4="horror";}
					if(chckbxRomance.isSelected()){movieTypes5="romance";} 
					if(chckbxThriller.isSelected()){movieTypes6="thriller";}
					
					//sport code
					if(chckbxSoccer.isSelected()){sports2="soccer";}
					if(chkbxRidinghorses.isSelected()){sports3="riding_horses";}
					if(chckbxBasketball.isSelected()){sports4="basketball";}
					if(chckbxVolleyball.isSelected()){sports5="volleyball";}
					if(chckbxdance.isSelected()){sports6="dance";}
					
					//fix null
					 if (music1 == null) { music1="";}
					 if (music2 == null) { music2="";}
					 if (music3 == null) { music3="";}
					 if (music4 == null) { music4="";}
					 if (music5 == null) { music5="";}
					 if (music6 == null) { music6="";}
					 if (music7 == null) { music7="";}
					 if (music8 == null) { music8="";}
				
					 if (movieTypes1 == null) { movieTypes1="";}
					 if (movieTypes2 == null) { movieTypes2="";}
					 if (movieTypes3 == null) { movieTypes3="";}
					 if (movieTypes4 == null) { movieTypes4="";}
					 if (movieTypes5 == null) { movieTypes5="";}
					 if (movieTypes6 == null) { movieTypes6="";}
					
					 if (sports1 == null) { sports1="";}
					 if (sports2 == null) { sports2="";}
					 if (sports3 == null) { sports3="";}
					 if (sports4 == null) { sports4="";}
					 if (sports5 == null) { sports5="";}
					 if (sports6 == null) { sports6="";}
					 if (sports7 == null) { sports7="";}
					 
					 
					 
					 
					 
					 
					music= music1+" "+music2+" "+music3+" "+music4+" "+music5+" "+music6+" "+music7+" "+music8+" "+tfmusic.getText().replaceAll("\\s+", " ");
					movieTypes=movieTypes1+" "+movieTypes2+" "+movieTypes3+" "+movieTypes4+" "+movieTypes5+" "+movieTypes6;
					sports=sports1+" "+sports2+" "+sports3+" "+sports4+" "+sports5+" "+sports6+" "+sports7+" "+tfOtherSports.getText().replaceAll("\\s+", " ");
					hobbies=txtWeWontDo.getText().replaceAll("\\s+", " ");
					
					//if(.isSelected()){}if(.isSelected()){}if(.isSelected()){}if(.isSelected()){}if(.isSelected()){}if(.isSelected()){}if(.isSelected()){}if(.isSelected()){}if(.isSelected()){}if(.isSelected()){}if(.isSelected()){}if(.isSelected()){}if(.isSelected()){}if(.isSelected()){}if(.isSelected()){}if(.isSelected()){}if(.isSelected()){}
					
					
					WriteSignUp.writeNewUser();
					if (WriteSignUp.isExistingFile() == (false)) {
						JOptionPane.showMessageDialog(null,
								"Payment details are accepted.\nUsername is " + WriteSignUp.getStrFilename()
										+ ".\nLogin to use your account", "Successfully registered to Dare2Date",
								JOptionPane.INFORMATION_MESSAGE);
						//VIP_Updater.automaticEmailUpdaterNewUser();
						
					setAutoNewuserupdater(true);
						SendMessage.writeNewMessage();
						MainFrame.setScreenNumber(3);
						panelManager.panelManagerI();
					}
					if (WriteSignUp.isExistingFile() == (true)) {
						JOptionPane.showMessageDialog(null,
								"Username already taken\nPlease choose a different Username.", "Error",
								JOptionPane.INFORMATION_MESSAGE);
						WriteSignUp.setExistingFile(false);
					}

				}

			}
		});

		
		
		
		
		txtEmail = new JTextField();
		txtEmail.setBounds(423, 142, 137, 20);
		add(txtEmail);
		txtEmail.setColumns(10);
		
		txtzipcode = new JTextField();
		txtzipcode.setBounds(169, 218, 137, 20);
		add(txtzipcode);
		txtzipcode.setColumns(10);
		
		City = new JTextField();
		City.setBounds(169, 193, 137, 20);
		add(City);
		City.setColumns(10);
		
		txtWeWontDo = new JTextField();
		txtWeWontDo.setHorizontalAlignment(SwingConstants.CENTER);
		txtWeWontDo.setText("We won't do all the work for you! Tell us something about yourself!");
		txtWeWontDo.setBounds(192, 553, 538, 70);
		add(txtWeWontDo);
		txtWeWontDo.setColumns(10);
		
		tfOtherSports = new JTextField();
		tfOtherSports.setBounds(291, 439, 427, 20);
		add(tfOtherSports);
		tfOtherSports.setColumns(10);
		
		tfcreditCard = new JTextField();
		tfcreditCard.setBounds(427, 271, 133, 20);
		add(tfcreditCard);
		tfcreditCard.setColumns(10);
		
		JLabel otherMusic = new JLabel("Other, namely:");
		otherMusic.setForeground(new Color(128, 0, 0));
		otherMusic.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		otherMusic.setBounds(182, 445, 154, 14);
		add(otherMusic);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		btnCancel.setBounds(435, 634, 89, 23);
		add(btnCancel);
		
		tfmusic = new JTextField();
		tfmusic.setBounds(291, 518, 427, 20);
		add(tfmusic);
		tfmusic.setColumns(10);
		
		textField = new JTextField();
		textField.setBounds(423, 193, 137, 20);
		add(textField);
		textField.setColumns(10);
		
		tfcountry = new JTextField();
		tfcountry.setBounds(632, 218, 137, 20);
		add(tfcountry);
		tfcountry.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(192, 164, 46, 20);
		add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(265, 164, 46, 20);
		add(textField_2);
		
		JLabel lblDay = new JLabel("DD:");
		lblDay.setBounds(166, 168, 24, 14);
		add(lblDay);
		
		JLabel lblMm = new JLabel("MM:");
		lblMm.setBounds(243, 167, 24, 14);
		add(lblMm);
		
		JLabel lblYear = new JLabel("YEAR:");
		lblYear.setBounds(316, 167, 46, 14);
		add(lblYear);
		
		
		
		btnCancel.addActionListener(new ActionListener() {

			
			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.print("cancel clikked......");

				MainFrame.setScreenNumber(3);
				panelManager.panelManagerI();
			}
		});
		
				


	}

	public static JTextField getTfcreditCard() {
		return tfcreditCard;
	}

	public void setTfcreditCard(JTextField tfcreditCard) {
		SignUpScreen_Panel2.tfcreditCard = tfcreditCard;
	}

	public static JTextField getAdrstfield() {
		return Adrstfield;
	}

	public static void setAdrstfield(JTextField adrstfield) {
		Adrstfield = adrstfield;}


	public static JTextField getTfname() {
		return tfname;
	}

	public static JTextField getTfsname() {
		return tfsname;
	}

	public static JPasswordField getPwdField() {
		return pwdField;
	}

	public static String getGenderx() {
		return genderx;
	}

	public static String getGenderxLiked() {
		return genderxLiked;
	}

	public static String getVIPstatus() {
		return VIPstatus;
	}

	public static String getMusic() {
		return music;
	}

	public static String getMovieTypes() {
		return movieTypes;
	}

	public static String getHobbies() {
		return hobbies;
	}

	public static String getSports() {
		return sports;
	}

	public static void setSports(String sports) {
		SignUpScreen_Panel2.sports = sports;
	}

	public static JTextField textField_4() {
		return textField_4;
	}

	public void setYearOfBirth(JTextField yearOfBirth) {
		textField_4 = yearOfBirth;
	}

	public static boolean isAutoNewuserupdater() {
		return autoNewuserupdater;
	}

	public static void setAutoNewuserupdater(boolean autoNewuserupdater) {
		SignUpScreen_Panel2.autoNewuserupdater = autoNewuserupdater;
	}
}
