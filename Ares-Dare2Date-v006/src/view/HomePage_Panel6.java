package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Color;

import javax.swing.JTabbedPane;

import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JList;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import model.ReadTXT4inbox;
import util.PanelManager;

public class HomePage_Panel6 extends JPanel {

	

	//private JTabbedPane tabbedPane;
	
	
	private Settings settingsPane;
	private ChangeSettings changeSettings ;
	private FindMylove findMylove;
	private Inbox inbox;
	private SendMessagePane sendMessage = new SendMessagePane();
	private Chat chat = new Chat();
	private About aknowledgements =new About();
	private static JTabbedPane tabbedPane;
	private static JLabel inboxlevel;
	private static String emailsize;
	private static JLabel lblIncomingChatRequest;
	
	/**
	 * Create the frame.
	 */
	public HomePage_Panel6(final PanelManager panelManager) {
		
		
		
		System.out
				.println("\n::::::::::::::::::::::::::::::Now in  Homepage panel6::::::::::::::::::::::::::\n");
		setLayout(null);
		
		emailsize="inbox has ["+ReadTXT4inbox.getNumberOfMessagesX()+"] mail.";
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 802, 701);
		add(panel);
		panel.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setToolTipText("Logout button. Click to logout");
		panel_1.setBackground(Color.WHITE);
		panel_1.setBounds(0, 0, 802, 106);
		panel.add(panel_1);
		panel_1.setLayout(null);
		
		 setLblIncomingChatRequest(new JLabel());
		getLblIncomingChatRequest().setFont(new Font("Tahoma", Font.BOLD, 11));
		getLblIncomingChatRequest().setForeground(Color.RED);
		getLblIncomingChatRequest().setBounds(22, 11, 166, 14);
		panel_1.add(getLblIncomingChatRequest());
		
		// Log out button
				JButton buttonLogOut = new JButton("Log me out");
				buttonLogOut.setForeground(new Color(128, 0, 0));
				buttonLogOut.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
				buttonLogOut.setBounds(660, 21, 111, 23);
				panel_1.add(buttonLogOut);
				buttonLogOut.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						System.out
								.print("\n\t\t****************LOGOUT CLICKED***************\n\t\t\t**System shutdown** ");
						System.exit(0);
					}

				});
				
				inboxlevel=new JLabel(emailsize);
				getInboxlevel().setForeground(new Color(139, 0, 0));
				getInboxlevel().setBounds(660, 61, 111, 23);
				panel_1.add(getInboxlevel());
				
		//JLabel lblDaredateHomepage = new JLabel("Dare2Date Homepage");
		JLabel lblDaredateHomepage = new JLabel();
		lblDaredateHomepage.setIcon(new ImageIcon("pic/loginscreenlogo.png"));
		
		lblDaredateHomepage.setForeground(new Color(139, 0, 0));
		lblDaredateHomepage.setFont(new Font("Script MT Bold", Font.BOLD, 40));
		lblDaredateHomepage.setBounds(0, 0, 792, 106);
		panel_1.add(lblDaredateHomepage);
	
		
	
			
			
			
		
		
		
		///////////////////////////////////////////
		
		
		
		
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.WHITE);
		panel_2.setBounds(0, 105, 802, 596);
		panel.add(panel_2);
		panel_2.setLayout(null);
		
	 setTabbedPane(new JTabbedPane(JTabbedPane.TOP));
		getTabbedPane().setBorder(new SoftBevelBorder(BevelBorder.LOWERED, new Color(128, 0, 0), new Color(178, 34, 34), new Color(165, 42, 42), new Color(205, 92, 92)));
		getTabbedPane().setBounds(10, 11, 770, 545);
		panel_2.add(getTabbedPane());
		
        
        settingsPane = new Settings();
        settingsPane.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, new Color(128, 0, 0), new Color(178, 34, 34), new Color(165, 42, 42), new Color(205, 92, 92)));
        settingsPane.setLayout(null);
        settingsPane.setBackground(Color.WHITE);
        
        changeSettings = new ChangeSettings();
        changeSettings.setBorder(new SoftBevelBorder(BevelBorder.LOWERED,new Color(128, 0, 0), new Color(178, 34, 34), new Color(165,42, 42), new Color(205, 92, 92)));
    	changeSettings.setLayout(null);
    	changeSettings.setBackground(Color.WHITE);
    	
    	findMylove = new FindMylove();
    	findMylove.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, new Color(128, 0, 0), new Color(178, 34, 34), new Color(165, 42, 42),new Color(205, 92, 92)));
    	findMylove.setLayout(null);
    	findMylove.setBackground(Color.WHITE);
    	
    	inbox= new Inbox();
    	inbox.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, new Color(128,0, 0), new Color(178, 34, 34), new Color(165, 42, 42),new Color(205, 92, 92)));
		inbox.setLayout(null);
		inbox.setBackground(Color.WHITE);
    
		chat= new Chat();
    	chat.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, new Color(128,0, 0), new Color(178, 34, 34), new Color(165, 42, 42),new Color(205, 92, 92)));
		chat.setLayout(null);
		chat.setBackground(Color.WHITE);
		
		sendMessage= new SendMessagePane();
		sendMessage.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, new Color(128,0, 0), new Color(178, 34, 34), new Color(165, 42, 42),new Color(205, 92, 92)));
		sendMessage.setLayout(null);
		sendMessage.setBackground(Color.WHITE);
		
		
		aknowledgements= new About();
		aknowledgements.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, new Color(128,0, 0), new Color(178, 34, 34), new Color(165, 42, 42),new Color(205, 92, 92)));
		aknowledgements.setLayout(null);
		aknowledgements.setBackground(Color.WHITE);
    	
        
        getTabbedPane().add("Settings", settingsPane);
        getTabbedPane().setForegroundAt(0, new Color(128, 0, 0));
        getTabbedPane().add("ChangeSettings", changeSettings);
        getTabbedPane().setForegroundAt(1, new Color(128, 0, 0));
        getTabbedPane().add("FindMylove",findMylove );
        getTabbedPane().setForegroundAt(2, new Color(128, 0, 0));
        getTabbedPane().add("Inbox",inbox );
        getTabbedPane().setForegroundAt(3, new Color(128, 0, 0));
        getTabbedPane().add("SendMessage", sendMessage);
        getTabbedPane().setForegroundAt(4, new Color(128, 0, 0));
        getTabbedPane().add("Chat",chat );
        getTabbedPane().setForegroundAt(5, new Color(128, 0, 0));
        getTabbedPane().add("About",aknowledgements );
        getTabbedPane().setForegroundAt(6, new Color(128, 0, 0));
     // tabbed pane event listener blah blah blah  !!!!!!!!
        getTabbedPane().addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				System.out.println("\n :=:=:=:=:=:  Tab index: "+ getTabbedPane().getSelectedIndex()+".:=:=:=:=:=:\n");
				if (getTabbedPane().getSelectedIndex() == 0) {
				}
				if (getTabbedPane().getSelectedIndex() == 1) {

				}
				if (getTabbedPane().getSelectedIndex() == 2) {
								
				}

				if (getTabbedPane().getSelectedIndex() == 3) {

				}
				if (getTabbedPane().getSelectedIndex() == 4) {
				}

				if (getTabbedPane().getSelectedIndex() == 5) {
					
					
				}
			}
		});
     
        
        
        
        
        
        
        
        

		 
	}

	public static JTabbedPane getTabbedPane() {
		return tabbedPane;
	}

	public void setTabbedPane(JTabbedPane tabbedPane) {
		this.tabbedPane = tabbedPane;
	}

	public static JLabel getInboxlevel() {
		return inboxlevel;
	}

	public void setInboxlevel(JLabel inboxlevel) {
		this.inboxlevel = inboxlevel;
	}

	public static JLabel getLblIncomingChatRequest() {
		return lblIncomingChatRequest;
	}

	public static void setLblIncomingChatRequest(JLabel lblIncomingChatRequest) {
		HomePage_Panel6.lblIncomingChatRequest = lblIncomingChatRequest;
	}
}
