package view;

import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.JTextArea;
import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

public class About extends JPanel{
	public About() {
		setBackground(Color.WHITE);
		setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(128, 0, 0));
		panel.setBounds(28, 31, 701, 460);
		add(panel);
		panel.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(10, 11, 681, 430);
		panel.add(scrollPane);
		
		JTextArea txtrSampleApplicationLicense = new JTextArea();
		txtrSampleApplicationLicense.setEditable(false);
		txtrSampleApplicationLicense.setForeground(new Color(178, 34, 34));
		txtrSampleApplicationLicense.setFont(new Font("Arial", Font.BOLD, 12));
		txtrSampleApplicationLicense.setBackground(Color.WHITE);
		txtrSampleApplicationLicense.setLineWrap(true);
		txtrSampleApplicationLicense.setText
		("This section contains:\r\n\r\n1) Author/Developer\u2019s note.\r\n2) "
				+ "Acknowledgements.\r\n3) Application License Agreement\r\n\r\n) "
				+ "Author/Developer\u2019s note.\r\n\r\n"
				+ "This application was created aspart of the solution for the dare2date case study \n "
				+ "which was given to us as part of the course Software Engineering project 2015\n"
				+ "(Inholland University of Applied Sciences Diemen). \n"
				+ "The other part of the solution is the workbook document. "
				+ "\r\nGroup3 is composed of the following members:\r\n\r\n"
				+ "a) N. Nkomo\r\n"
				+ "b) N. Can\r\nc) E. Fernandez\r\n"
				+ "d) S. Alnajar\r\n\r\n"
				+ "2) Acknowledgements \r\n\r\n"
				+ "In this section we, the Group3 authors, would like to thank all those who assisted in the making\n"
				+ "of this application and in particular our lectures who guided us through this project \n"
				+ "and those we consulted for specific area consultation.\r\n\r\n\r\n"
				+ "3) Application License Agreement. \r\n\r\n\r\n"
				+ "By downloading or using this software or accompanying documentation you agree \n"
				+ "to the following terms and conditions.\r\n "
				+ "\r\n"
				+ "This application will remain property of Group3 (Inholland Diemen year 2, 2015) \n"
				+ "until passed to the client, at which point the client license will \n"
				+ "replace existing license.\r\n\r\n"
				+ "This application has been created for educational purposes as part of the course Software engineering project \n"
				+ "and is part of the solution to the Dare2date case study given as an assignment.\r\n"
				+ "Members of group3 who were part of this project and have the right to this application\n"
				+ "are the following:\r\n"
				+ "1) Nelson N Nkomo\r\n"
				+ "2) Nilu Can\r\n"
				+ "3) Enoch Fernandez\r\n"
				+ "4) Samar Alnajar\r\n"
				+ "Only these members have the right to distribute, edit, or amend the license of this application.\r\n\r\n"
				+ "You are hereby NOT granted a personal, non-transferable and non-sublicense able, \n"
				+ "nonexclusive, world-wide,\n"
				+ "royalty free copyright or any kind of license to reproduce, prepare derivative works of, \n"
				+ "publicly display, publicly perform, distribute and sublicense the program examples \n"
				+ "and sample applications\n"
				+ "and any such derivative works, in source code and object code form.\n\n"
				+ "Except for the copyright license above, you are granted no other rights or licenses, \n"
				+ "by implication, or estoppel, or otherwise, under any patents or other intellectual property\n"
				+ "rights.\n"
				+ "Only Group 3 members stated above have the right to change the above license point.\r\n \r\n"
				+ "No Warranties.  The Sample Applications contained herein are provided on \n"
				+ "an \"AS IS\" basis \n"
				+ "and to the maximum extent permitted by applicable law, this material is \n"
				+ "provided AS IS AND WITH ALL FAULTS,\n"
				+ "and the authors and developers of this material and Group3 hereby disclaim \n"
				+ "all other warranties and conditions,\n"
				+ "either express, implied or statutory, including, but not limited to, \n"
				+ "any (if any) implied warranties,\n"
				+ "duties or conditions of merchantability, of fitness for a particular purpose,\n"
				+ "of accuracy or completeness of responses, of results, of workmanlike effort,\n"
				+ "of lack of viruses, and of lack of negligence. ALSO, THERE IS NO WARRANTY OR CONDITION \n"
				+ "OF TITLE,\n"
				+ "QUIET ENJOYMENT, QUIET POSSESSION, AND CORRESPONDENCE TO DESCRIPTION OR NON-INFRINGEMENT\n"
				+ " WITH REGARD TO THIS MATERIAL.\r\n \r\n"
				+ "Limitation of Liability.  IN NO EVENT WILL ANY AUTHOR, DEVELOPER, LICENSOR,\n"
				+ "OR DISTRIBUTOR OF THIS MATERIAL OR Group3 BE LIABLE TO ANY OTHER PARTY FOR \n"
				+ "THE COST OF PROCURING SUBSTITUTE GOODS OR SERVICES,\n"
				+ "LOST PROFITS, LOSS OF USE, LOSS OF DATA, OR ANY INCIDENTAL, CONSEQUENTIAL, \n"
				+ "DIRECT, INDIRECT, PUNITIVE,\n"
				+ "OR SPECIAL DAMAGES WHETHER UNDER CONTRACT, TORT, WARRANTY, OR OTHERWISE, \n"
				+ "ARISING IN ANY WAY OUT OF THIS \n"
				+ "OR ANY OTHER AGREEMENT RELATING TO THIS MATERIAL, WHETHER OR NOT SUCH PARTY \n"
				+ "HAD ADVANCE NOTICE OF THE POSSIBILITY\n"
				+ "OF SUCH DAMAGES.\r\n \r\n"
				+ "Redistribution. If you choose to distribute these Sample Applications or \n"
				+ "any derivative works thereof in a commercial product,\n"
				+ "you must defend and indemnify all authors, developers, licensors, and distributors \n"
				+ "(the \"Indemnified Parties\") of the Sample Applications against any losses,\n"
				+ "damages and costs arising from claims, lawsuits and other legal actions \n"
				+ "(excluding actions based on intellectual property infringement claims) brought\n"
				+ "by a third party against the Indemnified Parties to the extent caused by \n"
				+ "your acts or omissions in connection with your distribution.\n"
				+ "Regardless of whether your distribution is a commercial product or not, \n"
				+ "the license under which you redistribute the Sample Applications\n"
				+ "or any derivative works thereof must:\r\n \r\neffectively disclaim on behalf of \n"
				+ "all authors, developers,\n"
				+ "licensors, and distributors all warranties and conditions, express and implied, \n"
				+ "including warranties or conditions\n"
				+ "of title and non-infringement, and implied warranties or conditions of \n"
				+ "merchantability and fitness for a particular purpose;\r\n"
				+ "effectively exclude on behalf of all authors, developers, licensors, and \n"
				+ "distributors all liability for damages,\n"
				+ "including direct, indirect, special, punitive, incidental and consequential damages, \n"
				+ "such as lost profits;\r\n"
				+ "State that any provisions which differ from this license are offered by you \n"
				+ "alone and not by any other party; and\r\n"
				+ "Require that the license under which any subsequent distribution of the Sample Applications \n"
				+ "or derivative works thereof is made satisfy\n"
				+ "the terms of this section.\r\n"
				+ "General.\r\n"
				+ "No other rights are granted by implication, estoppel or otherwise.\r\n \r\n"
				+ "If any provision of this Agreement is invalid or unenforceable under applicable law,\n"
				+ "it shall not affect the validity or enforceability\n"
				+ "of the remainder of the terms of this Agreement, and without further action by the parties\n"
				+ "hereto, such provision shall be\n"
				+ "reformed to the minimum extent necessary to make such provision valid and enforceable.\r\n \r\n"
				+ "Your rights under this Agreement shall terminate if you fail to comply with any of \n"
				+ "the material terms or conditions of this Agreement and do not cure such failure in a \n"
				+ "reasonable period of time after becoming aware of such noncompliance.  \n"
				+ "If all your rights under this Agreement terminate, you agree to cease use of the Sample \n"
				+ "Applications and any derivative works thereof immediately.\r\n\r\n");
		//this tels the scroll pane to start at the top
		txtrSampleApplicationLicense.setCaretPosition(0); 
		scrollPane.setViewportView(txtrSampleApplicationLicense);
	}
}
