package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import controller.MainFrame;
import util.FileUtilReplacer;
import util.PanelManager;
//import model.PathMaker;
//import model.PathMaker;
import model.ReadTxt4Login;
import model.SendMessage;

public class ChangeSettings extends JPanel {

	private static String userFirstname = "blank";
	private static String userSurname = "blank";
	private static String addressH = "blank";
	private static String creditCardNumberH = "blank";
	private static String passwordH = "blank";
	private static String blankID = "blank";
	private static String blankDOB = "blank";
	private static String blankSP = "blank";
	private static String blankMT = "blank";
	private static String NumberOfMessages = "0";
	// ReadTXT4inbox.getNumberOfMessagesX();
	private static String blankMusic = "blank";
	private static String blankHobbies = "blank";
	private static String blankMovies = "blank";
	private static String blankGender = "blank";
	private static String blankSports = "blank";
	private static boolean modelLoaded = false;
	private static boolean modelLoadedVOUP = false;
	private static JTextField txtNewpwd;
	private static JTextField txtNewcrdtcdnum;
	private static JTextField txtNewcity;
	private static boolean chngePwd = false;
	private static boolean chgeSport = false;

	private static JRadioButton rdbtnVip1;
	private static JRadioButton rdbtnRegular1;
	private static JRadioButton rdbtnMan;
	private static JRadioButton rdbtnWomen;
	private static JRadioButton rdbtnOther;
	private static JTextField tfNewMusic;
	private static JTextField tfNewHobbies;
	private static JTextField tfNewMovies;
	private static JTextField tfNewSport;

	// inbox variables
	public static boolean secondMsgDel = false;
	public static boolean firstMsgDel = false;
	public static boolean thirdMsgDel;
	public static boolean fourthMsgDel;
	public static boolean fifthMsgDel;
	private static String sendrID = ReadTxt4Login.getLine1();
	private static String sendrIDVOUP = ReadTxt4Login.getLine1();
	private static boolean autoupdater = false;
	private static JLabel address1;
	private static JLabel lblBlank;
	private static JLabel lblBlank_1;
	private static JLabel Movies1;
	private static JLabel sport1;
	private static JLabel Music1;
	private static JLabel Hobbies1;
	private static JLabel creditc1;
	private static JLabel password1;
	private static boolean chngeSX;
	private static boolean chngeCrdtCdNum;
	private static boolean chngeCity;
	private static boolean changeMembertype;
	private static boolean chgeMusic;
	private static boolean chgeHobbies;
	private static boolean chgeMovies;

	public ChangeSettings() {

		JLabel label_36 = new JLabel("UserID:");
		label_36.setForeground(new Color(128, 0, 0));
		label_36.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		label_36.setBounds(32, 61, 98, 14);
		add(label_36);

		JLabel label_37 = new JLabel("First name:");
		label_37.setForeground(new Color(128, 0, 0));
		label_37.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		label_37.setBounds(32, 83, 98, 14);
		add(label_37);

		JLabel label_38 = new JLabel("Last name:");
		label_38.setForeground(new Color(128, 0, 0));
		label_38.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		label_38.setBounds(32, 108, 98, 14);
		add(label_38);

		JLabel label_40 = new JLabel("Creditcard:");
		label_40.setForeground(new Color(128, 0, 0));
		label_40.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		label_40.setBounds(32, 230, 98, 14);
		add(label_40);

		JLabel label_42 = new JLabel("City:");
		label_42.setForeground(new Color(128, 0, 0));
		label_42.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		label_42.setBounds(32, 133, 98, 14);
		// label_42.setBounds(492, 158, 98, 14);
		add(label_42);

		JLabel label_45 = new JLabel("Gender:");
		label_45.setForeground(new Color(128, 0, 0));
		label_45.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		label_45.setBounds(32, 183, 98, 14);
		add(label_45);

		JLabel label_46 = new JLabel("Membership:");
		label_46.setForeground(new Color(128, 0, 0));
		label_46.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		label_46.setBounds(31, 282, 98, 14);
		add(label_46);

		JLabel label_47 = new JLabel("Password:");
		label_47.setForeground(new Color(128, 0, 0));
		label_47.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		label_47.setBounds(32, 206, 98, 14);
		add(label_47);

		JLabel label_48 = new JLabel("Sexual preference:");
		label_48.setForeground(new Color(128, 0, 0));
		label_48.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		label_48.setBounds(32, 256, 137, 14);
		add(label_48);

		JLabel label_49 = new JLabel("Movies:");
		label_49.setForeground(new Color(128, 0, 0));
		label_49.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		label_49.setBounds(32, 336, 98, 14);
		add(label_49);

		JLabel label_50 = new JLabel("Sports:");
		label_50.setForeground(new Color(128, 0, 0));
		label_50.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		label_50.setBounds(32, 361, 137, 14);
		add(label_50);

		JLabel label_51 = new JLabel("Music:");
		label_51.setForeground(new Color(128, 0, 0));
		label_51.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		label_51.setBounds(32, 386, 98, 14);
		add(label_51);

		JLabel label_52 = new JLabel("Hobbies:");
		label_52.setForeground(new Color(128, 0, 0));
		label_52.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		label_52.setBounds(32, 411, 98, 14);
		add(label_52);

		// Blank of the settings changed tab.
		JLabel Userid1 = new JLabel(blankID);
		Userid1.setBounds(152, 61, 89, 14);
		add(Userid1);

		JLabel firstname1 = new JLabel(userFirstname);
		firstname1.setBounds(152, 83, 89, 14);
		add(firstname1);

		JLabel lastname1 = new JLabel(userSurname);
		lastname1.setBounds(152, 108, 89, 14);
		add(lastname1);

		address1=new JLabel(addressH);
		getAddress1().setBounds(152, 133, 95, 14);
		add(getAddress1());

		JLabel gender1 = new JLabel(blankGender);
		gender1.setBounds(152, 183, 50, 14);
		add(gender1);

		password1=new JLabel(passwordH);
		getPassword1().setBounds(152, 206, 89, 14);
		add(getPassword1());

		Movies1 = new JLabel(blankMovies);
		Movies1.setBounds(152, 336, 300, 14);
		add(Movies1);

		sport1 = new JLabel(blankSports);
		sport1.setBounds(152, 361, 300, 14);
		add(sport1);

		Music1 = new JLabel(blankMusic);
		Music1.setBounds(152, 386, 300, 14);
		add(Music1);

		Hobbies1 = new JLabel(blankHobbies);
		Hobbies1.setBounds(152, 411, 300, 14);
		add(Hobbies1);

		creditc1 = new JLabel(creditCardNumberH);
		creditc1.setBounds(152, 230, 150, 14);
		add(creditc1);

		JLabel dob = new JLabel(blankDOB);
		dob.setBounds(152, 158, 47, 14);
		add(dob);

		JLabel label_68 = new JLabel("BirthYear:");
		label_68.setForeground(new Color(128, 0, 0));
		label_68.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		label_68.setBounds(32, 158, 89, 14);
		// label_68.setBounds(492, 183, 89, 14);
		add(label_68);

		lblBlank = new JLabel(blankSP);
		lblBlank.setBounds(152, 256, 90, 14);
		add(lblBlank);

		lblBlank_1 = new JLabel(blankMT);
		lblBlank_1.setBounds(152, 282, 90, 14);
		add(lblBlank_1);

		JButton button_C = new JButton("Change City");
		// button_4.setBackground(new Color(178, 34, 34));
		button_C.setForeground(new Color(178, 34, 34));
		button_C.setFont(new Font("Yu Gothic Light", Font.BOLD, 10));
		button_C.setBounds(409, 130, 200, 19);
		add(button_C);

		button_C.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {	if (ReadTxt4Login.getLine9().equals("regular")) {

				JOptionPane
						.showMessageDialog(
								null,
								"Change to a VIP membership in order to edit this preference",
								"Notification",
								JOptionPane.INFORMATION_MESSAGE);

			}
			if (!ReadTxt4Login.getLine9().equals("regular")) {
				System.out
						.print("\n\t\t****************Change  address clicked***************\n ");

				setChngePwd(false);
				setChngeSX(false);
				setChngeCrdtCdNum(false);
				setChngeCity(true);

				FileUtilReplacer util = new FileUtilReplacer();

				util.removeLineFromFile(
						"Text-Files/" + ReadTxt4Login.getUsernameEntered()
								+ ".txt", getAddressH());
				address1.setText(FileUtilReplacer.getStuff());
				Settings.lblAdres.setText(FileUtilReplacer.getStuff());

				setAutoupdater(true);
				SendMessage.writeNewMessage();

				// setAutoupdater(true);SendMessage.writeNewMessage();

				// MainFrame.setScreenNumber(4);
				// MainFrame.panelManagerI();

			}}

		});

		JButton button_4 = new JButton("Change Membership");
		// button_4.setBackground(new Color(178, 34, 34));
		button_4.setForeground(new Color(178, 34, 34));
		button_4.setFont(new Font("Yu Gothic Light", Font.BOLD, 10));
		button_4.setBounds(409, 276, 200, 19);
		add(button_4);
		button_4.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (ReadTxt4Login.getLine9().equals("regular")) {

					JOptionPane
							.showMessageDialog(
									null,
									"Thank you for changing to VIP. A VIP CHANGE will\n be automatically charged to your credit card",
									"Notification",
									JOptionPane.INFORMATION_MESSAGE);

				}
				if (!ReadTxt4Login.getLine9().equals("regular")) {

					JOptionPane.showMessageDialog(null,
							"We are sorry that you feel you have to downgrade",
							"Notification", JOptionPane.INFORMATION_MESSAGE);

				}
				System.out
						.print("\n\t\t****************Upgrade plan***************\n ");

				setChngePwd(false);
				setChngeSX(false);
				setChngeCrdtCdNum(false);
				setChngeCity(false);
				setChangeMembertype(true);

				FileUtilReplacer util = new FileUtilReplacer();

				util.removeLineFromFile(
						"Text-Files/" + ReadTxt4Login.getUsernameEntered()
								+ ".txt", blankMT);
				lblBlank_1.setText((FileUtilReplacer.getStuff()));
				Settings. getLblBlankmt().setText((FileUtilReplacer.getStuff()));
				setAutoupdater(true);
				SendMessage.writeNewMessage();
				// MainFrame.setScreenNumber(4);
				// panelManager.panelManagerI();

			}

		});

		JButton button_5 = new JButton("Change Password");
		// button_5.setBackground(new Color(178, 34, 34));
		button_5.setForeground(new Color(178, 34, 34));
		button_5.setFont(new Font("Yu Gothic Light", Font.BOLD, 10));
		button_5.setBounds(409, 204, 200, 19);
		add(button_5);
		button_5.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				setChngePwd(true);
				setChngeSX(false);
				setChngeCrdtCdNum(false);
				setChngeCity(false);

				/*
				 * chngePwd=false; chngeSX=false; chngeCrdtCdNum=false;
				 * chngeCity=false;
				 */

				System.out
						.print("\n\t\t****************Change pswd clicked***************\n ");

				FileUtilReplacer util = new FileUtilReplacer();

				util.removeLineFromFile(
						"Text-Files/" + ReadTxt4Login.getUsernameEntered()
								+ ".txt", getPasswordH());
				password1.setText(FileUtilReplacer.getStuff());
				
				Settings.getLblPasswordll().setText(FileUtilReplacer.getStuff());
				
				
				// setPasswordH().setText;

			}

		});

		JButton buttonChangeMovies = new JButton("Change Movies");
		// buttonChangeMovies.setBackground(new Color(178, 34, 34));
		buttonChangeMovies.setForeground(new Color(178, 34, 34));
		buttonChangeMovies.setFont(new Font("Yu Gothic Light", Font.BOLD, 10));
		buttonChangeMovies.setBounds(409, 331, 200, 19);
		add(buttonChangeMovies);
		buttonChangeMovies.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				if (ReadTxt4Login.getLine9().equals("regular")) {

					JOptionPane
							.showMessageDialog(
									null,
									"Change to a VIP membership in order to edit this preference",
									"Notification",
									JOptionPane.INFORMATION_MESSAGE);

				}
				if (!ReadTxt4Login.getLine9().equals("regular")) {
				System.out
						.print("\n\t\t****************Change  Movies clicked***************\n ");

				setChgeMusic(false);
				setChgeHobbies(false);
				setChgeMovies(true);
				setChgeSport(false);

				setChngePwd(false);
				setChngeSX(false);
				setChngeCrdtCdNum(false);
				setChngeCity(false);

				FileUtilReplacer util = new FileUtilReplacer();

				util.removeLineFromFile(
						("Text-Files/" + ReadTxt4Login.getUsernameEntered() + ".txt"),
						getBlankMovies());
				Movies1.setText(FileUtilReplacer.getStuff());
				Settings.getLblBlankmovies().setText(FileUtilReplacer.getStuff());

				setAutoupdater(true);
				SendMessage.writeNewMessage();

			}}

		});

		JButton buttonChangeSports = new JButton("Change Sports");
		// buttonChangeSports.setBackground(new Color(178, 34, 34));
		buttonChangeSports.setForeground(new Color(178, 34, 34));
		buttonChangeSports.setFont(new Font("Yu Gothic Light", Font.BOLD, 10));
		// buttonChangeSports.setBounds(620, 331, 79, 19);
		buttonChangeSports.setBounds(409, 356, 200, 19);
		add(buttonChangeSports);

		buttonChangeSports.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {	if (ReadTxt4Login.getLine9().equals("regular")) {

				JOptionPane
						.showMessageDialog(
								null,
								"Change to a VIP membership in order to edit this preference",
								"Notification",
								JOptionPane.INFORMATION_MESSAGE);

			}
			if (!ReadTxt4Login.getLine9().equals("regular")) {
				System.out
						.print("\n\t\t****************Change  sports clicked***************\n ");

				setChgeMusic(false);
				setChgeHobbies(false);
				setChgeMovies(false);
				setChgeSport(true);

				setChngePwd(false);
				setChngeSX(false);
				setChngeCrdtCdNum(false);
				setChngeCity(false);

				FileUtilReplacer util = new FileUtilReplacer();

				util.removeLineFromFile(
						"Text-Files/" + ReadTxt4Login.getUsernameEntered()
								+ ".txt", getBlankSports());
				sport1.setText(FileUtilReplacer.getStuff());
				Settings.getLblBlanksports().setText(FileUtilReplacer.getStuff());

				setAutoupdater(true);
				SendMessage.writeNewMessage();

			}}
		});

		JButton buttonChangeMusic = new JButton("Change Music");
		// buttonChangeMusic.setBackground(new Color(178, 34, 34));
		buttonChangeMusic.setForeground(new Color(178, 34, 34));
		buttonChangeMusic.setFont(new Font("Yu Gothic Light", Font.BOLD, 10));
		buttonChangeMusic.setBounds(409, 381, 200, 19);
		add(buttonChangeMusic);

		buttonChangeMusic.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (ReadTxt4Login.getLine9().equals("regular")) {

					JOptionPane
							.showMessageDialog(
									null,
									"Change to a VIP membership in order to edit this preference",
									"Notification",
									JOptionPane.INFORMATION_MESSAGE);

				}
				if (!ReadTxt4Login.getLine9().equals("regular")) {
				System.out
						.print("\n\t\t****************Change  music clicked***************\n ");

				setChgeMusic(true);
				setChgeHobbies(false);
				setChgeMovies(false);
				setChgeSport(false);

				// JLabel lblBlankmusic = new JLabel(blankMusic);

				setChngePwd(false);
				setChngeSX(false);
				setChngeCrdtCdNum(false);
				setChngeCity(false);

				FileUtilReplacer util = new FileUtilReplacer();

				util.removeLineFromFile(
						"Text-Files/" + ReadTxt4Login.getUsernameEntered()
								+ ".txt", getBlankMusic());
				Music1.setText(FileUtilReplacer.getStuff());
				Settings.getLblBlankmusic().setText(FileUtilReplacer.getStuff());
				setAutoupdater(true);
				SendMessage.writeNewMessage();

			}}

		});

		JButton buttongChangeHobbies = new JButton("Change Hobbies");
		// buttongChangeHobbies.setBackground(new Color(178, 34, 34));
		buttongChangeHobbies.setForeground(new Color(178, 34, 34));
		buttongChangeHobbies
				.setFont(new Font("Yu Gothic Light", Font.BOLD, 10));
		buttongChangeHobbies.setBounds(409, 406, 200, 19);
		add(buttongChangeHobbies);

		buttongChangeHobbies.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				
				
				
				
				if (ReadTxt4Login.getLine9().equals("regular")) {

					JOptionPane
							.showMessageDialog(
									null,
									"Change to a VIP membership in order to edit this preference",
									"Notification",
									JOptionPane.INFORMATION_MESSAGE);

				}
				if (!ReadTxt4Login.getLine9().equals("regular")) {
				System.out
						.print("\n\t\t****************Change  hobbies clicked***************\n ");

				setChgeMusic(false);
				setChgeHobbies(true);
				setChgeMovies(false);
				setChgeSport(false);

				setChngePwd(false);
				setChngeSX(false);
				setChngeCrdtCdNum(false);
				setChngeCity(false);

				FileUtilReplacer util = new FileUtilReplacer();

				util.removeLineFromFile(
						"Text-Files/" + ReadTxt4Login.getUsernameEntered()
								+ ".txt", getBlankHobbies());
				Hobbies1.setText(FileUtilReplacer.getStuff());
				Settings.getLblBlankhobbies().setText(FileUtilReplacer.getStuff());
				setAutoupdater(true);
				SendMessage.writeNewMessage();
				System.out.print(" \n============\n\n>>>>>>>>>ch hobbies"+FileUtilReplacer.getStuff()+"<<<<<<<<\n\n");
			}}

		});

		JButton button_10 = new JButton("Change Sexual Preferences");
		// button_10.setBackground(new Color(178, 34, 34));
		button_10.setForeground(new Color(178, 34, 34));
		button_10.setFont(new Font("Yu Gothic Light", Font.BOLD, 10));
		button_10.setBounds(409, 252, 200, 19);
		add(button_10);
		button_10.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (ReadTxt4Login.getLine9().equals("regular")) {

					JOptionPane
							.showMessageDialog(
									null,
									"Change to a VIP membership in order to edit this preference",
									"Notification",
									JOptionPane.INFORMATION_MESSAGE);

				}
				if (!ReadTxt4Login.getLine9().equals("regular")) {
					System.out
							.print("\n\t\t****************Change Sexual preference clicked***************\n ");

					setChngePwd(false);
					setChngeSX(true);
					setChngeCrdtCdNum(false);
					setChngeCity(false);
					setChangeMembertype(false);

					FileUtilReplacer util = new FileUtilReplacer();

					util.removeLineFromFile(
							"Text-Files/" + ReadTxt4Login.getUsernameEntered()
									+ ".txt", blankSP);
					lblBlank.setText(FileUtilReplacer.getStuff());
					Settings.getLblBlanksp().setText(FileUtilReplacer.getStuff());
					System.out.print(" \n============\n\n>>>>>>>>>ch sp"+FileUtilReplacer.getStuff()+"<<<<<<<<\n\n");
					setAutoupdater(true);
					SendMessage.writeNewMessage();
					// MainFrame.setScreenNumber(4);
					// panelManager.panelManagerI();
					System.out.println("============REPLACE SP RUN=========");
				}
			}

		});

		JButton button_14 = new JButton("Change Creditcard Number");
		// button_14.setBackground(new Color(178, 34, 34));
		button_14.setForeground(new Color(178, 34, 34));
		button_14.setFont(new Font("Yu Gothic Light", Font.BOLD, 10));
		button_14.setBounds(409, 227, 200, 20);
		add(button_14);
		button_14.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.out
						.print("\n\t\t****************CHANGE CARD clicked***************\n ");

				setChngePwd(false);
				setChngeSX(false);
				setChngeCrdtCdNum(true);
				setChngeCity(false);

				FileUtilReplacer util = new FileUtilReplacer();

				util.removeLineFromFile(
						"Text-Files/" + ReadTxt4Login.getUsernameEntered()
								+ ".txt", getCreditCardNumberH());
				creditc1.setText(FileUtilReplacer.getStuff());
				Settings.getLblCrnumber().setText(FileUtilReplacer.getStuff());
				System.out.print(" \n============\n\n>>>>>>>>>ch card"+FileUtilReplacer.getStuff()+"<<<<<<<<\n\n");
				setAutoupdater(true);
				SendMessage.writeNewMessage();
				// MainFrame.setScreenNumber(4);
				// panelManager.panelManagerI();

			}

		});

		setRdbtnMan(new JRadioButton("man"));
		getRdbtnMan().setBackground(new Color(255, 255, 255));
		getRdbtnMan().setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		getRdbtnMan().setBounds(339, 252, 51, 23);
		add(getRdbtnMan());

		setRdbtnWomen(new JRadioButton("women"));
		getRdbtnWomen().setBackground(new Color(255, 255, 255));
		getRdbtnWomen().setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		getRdbtnWomen().setBounds(250, 252, 70, 23);
		add(getRdbtnWomen());

		// collect from sexual peference radio buttons
		ButtonGroup SexualPreference = new ButtonGroup();
		SexualPreference.add(getRdbtnOther());
		SexualPreference.add(getRdbtnWomen());
		SexualPreference.add(getRdbtnMan());

		setRdbtnRegular1(new JRadioButton("regular"));
		getRdbtnRegular1().setBackground(new Color(255, 255, 255));
		getRdbtnRegular1().setBounds(250, 278, 69, 23);
		add(getRdbtnRegular1());

		setRdbtnVip1(new JRadioButton("VIP"));
		getRdbtnVip1().setBackground(new Color(255, 255, 255));
		getRdbtnVip1().setBounds(339, 278, 49, 23);
		add(getRdbtnVip1());

		// collect from radion buttons vip vs regular
		ButtonGroup VIP_reguler = new ButtonGroup();
		VIP_reguler.add(getRdbtnVip1());
		VIP_reguler.add(getRdbtnRegular1());

		setTxtNewcity(new JTextField());
		getTxtNewcity().setText("New City");
		getTxtNewcity().setForeground(new Color(178, 34, 34));
		getTxtNewcity().setFont(new Font("Yu Gothic Light", Font.PLAIN, 12));
		getTxtNewcity().setBackground(new Color(255, 255, 255));
		getTxtNewcity().setBounds(250, 130, 140, 20);
		getTxtNewcity().setColumns(10);
		add(getTxtNewcity());

		setTxtNewcrdtcdnum(new JTextField());
		getTxtNewcrdtcdnum().setText("New Creditcardnumber");
		getTxtNewcrdtcdnum().setForeground(new Color(178, 34, 34));
		getTxtNewcrdtcdnum().setFont(new Font("Yu Gothic Light", Font.PLAIN, 12));
		getTxtNewcrdtcdnum().setBackground(new Color(255, 255, 255));
		getTxtNewcrdtcdnum().setBounds(250, 227, 140, 20);
		add(getTxtNewcrdtcdnum());
		getTxtNewcrdtcdnum().setColumns(10);

		setTfNewMovies(new JTextField());
		getTfNewMovies().setText("New Movies");
		getTfNewMovies().setForeground(new Color(178, 34, 34));
		getTfNewMovies().setFont(new Font("Yu Gothic Light", Font.PLAIN, 12));
		getTfNewMovies().setBackground(new Color(255, 255, 255));
		getTfNewMovies().setBounds(250, 333, 140, 20);
		add(getTfNewMovies());
		getTfNewMovies().setColumns(10);

		setTfNewSport(new JTextField());
		getTfNewSport().setText("New Sports");
		getTfNewSport().setForeground(new Color(178, 34, 34));
		getTfNewSport().setFont(new Font("Yu Gothic Light", Font.PLAIN, 12));
		getTfNewSport().setBackground(new Color(255, 255, 255));
		getTfNewSport().setBounds(250, 358, 140, 20);
		add(getTfNewSport());
		getTfNewSport().setColumns(10);

		setTfNewMusic(new JTextField());
		getTfNewMusic().setText("New Music");
		getTfNewMusic().setForeground(new Color(178, 34, 34));
		getTfNewMusic().setFont(new Font("Yu Gothic Light", Font.PLAIN, 12));
		getTfNewMusic().setBackground(new Color(255, 255, 255));
		getTfNewMusic().setBounds(250, 383, 140, 20);
		add(getTfNewMusic());
		getTfNewMusic().setColumns(10);

		setTfNewHobbies(new JTextField());
		getTfNewHobbies().setText("New Hobbies");
		getTfNewHobbies().setForeground(new Color(178, 34, 34));
		getTfNewHobbies().setFont(new Font("Yu Gothic Light", Font.PLAIN, 12));
		getTfNewHobbies().setBackground(new Color(255, 255, 255));
		getTfNewHobbies().setBounds(250, 408, 140, 20);
		add(getTfNewHobbies());
		getTfNewHobbies().setColumns(10);

		setTxtNewpwd(new JTextField());
		getTxtNewpwd().setText("New Password");
		getTxtNewpwd().setForeground(new Color(178, 34, 34));
		getTxtNewpwd().setFont(new Font("Yu Gothic Light", Font.PLAIN, 12));
		getTxtNewpwd().setBackground(new Color(255, 255, 255));
		getTxtNewpwd().setBounds(250, 204, 140, 20);
		add(getTxtNewpwd());
		getTxtNewpwd().setColumns(10);

		// ==================================

	}

	public static void setUserFirstname(String userFirstname) {
		ChangeSettings.userFirstname = userFirstname;
	}

	public static void setUserSurname(String userSurname) {
		ChangeSettings.userSurname = userSurname;
	}

	public static void setAddressH(String addressH) {
		ChangeSettings.addressH = addressH;
	}

	public static void setCreditCardNumberH(String creditCardNumberH) {
		ChangeSettings.creditCardNumberH = creditCardNumberH;
	}

	public static void setPasswordH(String passwordH) {
		ChangeSettings.passwordH = passwordH;
	}

	public static void setBlankID(String blankID) {
		ChangeSettings.blankID = blankID;
	}

	public static void setBlankDOB(String blankDOB) {
		ChangeSettings.blankDOB = blankDOB;
	}

	public static void setBlankSP(String blankSP) {
		ChangeSettings.blankSP = blankSP;
	}

	public static void setBlankMT(String blankMT) {
		ChangeSettings.blankMT = blankMT;
	}

	public static void setNumberOfMessages(String numberOfMessages) {
		NumberOfMessages = numberOfMessages;
	}

	public static void setBlankMusic(String blankMusic) {
		ChangeSettings.blankMusic = blankMusic;
	}

	public static void setBlankHobbies(String blankHobbies) {
		ChangeSettings.blankHobbies = blankHobbies;
	}

	public static void setBlankMovies(String blankMovies) {
		ChangeSettings.blankMovies = blankMovies;
	}

	public static void setBlankGender(String blankGender) {
		ChangeSettings.blankGender = blankGender;
	}

	public static void setBlankSports(String blankSports) {
		ChangeSettings.blankSports = blankSports;
	}

	public static void setModelLoaded(boolean modelLoaded) {
		ChangeSettings.modelLoaded = modelLoaded;
	}

	public static void setModelLoadedVOUP(boolean modelLoadedVOUP) {
		ChangeSettings.modelLoadedVOUP = modelLoadedVOUP;
	}

	public static void setTxtNewpwd(JTextField txtNewpwd) {
		ChangeSettings.txtNewpwd = txtNewpwd;
	}

	public static void setTxtNewcrdtcdnum(JTextField txtNewcrdtcdnum) {
		ChangeSettings.txtNewcrdtcdnum = txtNewcrdtcdnum;
	}

	public static void setTxtNewcity(JTextField txtNewcity) {
		ChangeSettings.txtNewcity = txtNewcity;
	}

	public static void setChngePwd(boolean chngePwd) {
		ChangeSettings.chngePwd = chngePwd;
	}

	public static void setChngeSX(boolean chngeSX) {
		ChangeSettings.chngeSX = chngeSX;
	}

	public static void setChngeCrdtCdNum(boolean chngeCrdtCdNum) {
		ChangeSettings.chngeCrdtCdNum = chngeCrdtCdNum;

	}

	public static void setChngeCity(boolean chngeCity) {
		ChangeSettings.chngeCity = chngeCity;
	}

	public static void setChangeMembertype(boolean changeMembertype) {
		ChangeSettings.changeMembertype = changeMembertype;
	}

	public static void setChgeMusic(boolean chgeMusic) {
		ChangeSettings.chgeMusic = chgeMusic;
	}

	public static void setChgeHobbies(boolean chgeHobbies) {
		ChangeSettings.chgeHobbies = chgeHobbies;
	}

	public static void setChgeMovies(boolean chgeMovies) {
		ChangeSettings.chgeMovies = chgeMovies;
	}

	public static void setChgeSport(boolean chgeSport) {
		ChangeSettings.chgeSport = chgeSport;
	}

	public static void setRdbtnVip1(JRadioButton rdbtnVip1) {
		ChangeSettings.rdbtnVip1 = rdbtnVip1;
	}

	public static void setRdbtnRegular1(JRadioButton rdbtnRegular1) {
		ChangeSettings.rdbtnRegular1 = rdbtnRegular1;
	}

	public static void setRdbtnMan(JRadioButton rdbtnMan) {
		ChangeSettings.rdbtnMan = rdbtnMan;
	}

	public static void setRdbtnWomen(JRadioButton rdbtnWomen) {
		ChangeSettings.rdbtnWomen = rdbtnWomen;
	}

	public static void setRdbtnOther(JRadioButton rdbtnOther) {
		ChangeSettings.rdbtnOther = rdbtnOther;
	}

	public static void setTfNewMusic(JTextField tfNewMusic) {
		ChangeSettings.tfNewMusic = tfNewMusic;
	}

	public static void setTfNewHobbies(JTextField tfNewHobbies) {
		ChangeSettings.tfNewHobbies = tfNewHobbies;
	}

	public static void setTfNewMovies(JTextField tfNewMovies) {
		ChangeSettings.tfNewMovies = tfNewMovies;
	}

	public static void setTfNewSport(JTextField tfNewSport) {
		ChangeSettings.tfNewSport = tfNewSport;
	}

	public static void setSecondMsgDel(boolean secondMsgDel) {
		ChangeSettings.secondMsgDel = secondMsgDel;
	}

	public static void setFirstMsgDel(boolean firstMsgDel) {
		ChangeSettings.firstMsgDel = firstMsgDel;
	}

	public static String getUserFirstname() {
		return userFirstname;
	}

	public static String getUserSurname() {
		return userSurname;
	}

	public static String getAddressH() {
		return addressH;
	}

	public static String getCreditCardNumberH() {
		return creditCardNumberH;
	}

	public static String getPasswordH() {
		return passwordH;
	}

	public static String getBlankID() {
		return blankID;
	}

	public static String getBlankDOB() {
		return blankDOB;
	}

	public static String getBlankSP() {
		return blankSP;
	}

	public static String getBlankMT() {
		return blankMT;
	}

	public static String getNumberOfMessages() {
		return NumberOfMessages;
	}

	public static String getBlankMusic() {
		return blankMusic;
	}

	public static String getBlankHobbies() {
		return blankHobbies;
	}

	public static String getBlankMovies() {
		return blankMovies;
	}

	public static String getBlankGender() {
		return blankGender;
	}

	public static String getBlankSports() {
		return blankSports;
	}

	public static boolean isModelLoaded() {
		return modelLoaded;
	}

	public static boolean isModelLoadedVOUP() {
		return modelLoadedVOUP;
	}

	public static JTextField getTxtNewpwd() {
		return txtNewpwd;
	}

	public static JTextField getTxtNewcrdtcdnum() {
		return txtNewcrdtcdnum;
	}

	public static JTextField getTxtNewcity() {
		return txtNewcity;
	}

	public static boolean isChngePwd() {
		return chngePwd;
	}

	public static boolean isChgeSport() {
		return chgeSport;
	}

	public static JRadioButton getRdbtnVip1() {
		return rdbtnVip1;
	}

	public static JRadioButton getRdbtnRegular1() {
		return rdbtnRegular1;
	}

	public static JRadioButton getRdbtnMan() {
		return rdbtnMan;
	}

	public static JRadioButton getRdbtnWomen() {
		return rdbtnWomen;
	}

	public static JRadioButton getRdbtnOther() {
		return rdbtnOther;
	}

	public static JTextField getTfNewMusic() {
		return tfNewMusic;
	}

	public static JTextField getTfNewHobbies() {
		return tfNewHobbies;
	}

	public static JTextField getTfNewMovies() {
		return tfNewMovies;
	}

	public static JTextField getTfNewSport() {
		return tfNewSport;
	}

	public static boolean isAutoupdater() {
		return autoupdater;
	}

	public static void setAutoupdater(boolean autoupdater) {
		ChangeSettings.autoupdater = autoupdater;
	}

	public static JLabel getAddress1() {
		return address1;
	}

	public static void setAddress1(JLabel address1) {
		ChangeSettings.address1 = address1;
	}

	public static JLabel getPassword1() {
		return password1;
	}

	public static void setPassword1(JLabel passsword1) {
		ChangeSettings.password1 = passsword1;
	}

	public static JLabel getCreditc1() {
		return creditc1;
	}

	public static void setCreditc1(JLabel creditc1) {
		ChangeSettings.creditc1 = creditc1;
	}

	public static boolean isChngeSX() {
		return chngeSX;
	}

	public static boolean isChngeCrdtCdNum() {
		return chngeCrdtCdNum;
	}

	public static boolean isChngeCity() {
		return chngeCity;
	}

	public static boolean isChgeMusic() {
		return chgeMusic;
	}

	public static boolean isChgeHobbies() {
		return chgeHobbies;
	}

	public static boolean isChgeMovies() {
		return chgeMovies;
	}

	public static boolean isThirdMsgDel() {
		return thirdMsgDel;
	}

	public static void setThirdMsgDel(boolean thirdMsgDel) {
		ChangeSettings.thirdMsgDel = thirdMsgDel;
	}

	public static boolean isFourthMsgDel() {
		return fourthMsgDel;
	}

	public static void setFourthMsgDel(boolean fourthMsgDel) {
		ChangeSettings.fourthMsgDel = fourthMsgDel;
	}

	public static boolean isFifthMsgDel() {
		return fifthMsgDel;
	}

	public static void setFifthMsgDel(boolean fifthMsgDel) {
		ChangeSettings.fifthMsgDel = fifthMsgDel;
	}

	public static String getSendrID() {
		return sendrID;
	}

	public static void setSendrID(String sendrID) {
		ChangeSettings.sendrID = sendrID;
	}

	public static String getSendrIDVOUP() {
		return sendrIDVOUP;
	}

	public static void setSendrIDVOUP(String sendrIDVOUP) {
		ChangeSettings.sendrIDVOUP = sendrIDVOUP;
	}

	public static JLabel getLblBlank() {
		return lblBlank;
	}

	public static void setLblBlank(JLabel lblBlank) {
		ChangeSettings.lblBlank = lblBlank;
	}

	public static JLabel getLblBlank_1() {
		return lblBlank_1;
	}

	public static void setLblBlank_1(JLabel lblBlank_1) {
		ChangeSettings.lblBlank_1 = lblBlank_1;
	}

	public static JLabel getMovies1() {
		return Movies1;
	}

	public static void setMovies1(JLabel movies1) {
		Movies1 = movies1;
	}

	public static JLabel getSport1() {
		return sport1;
	}

	public static void setSport1(JLabel sport1) {
		ChangeSettings.sport1 = sport1;
	}

	public static JLabel getMusic1() {
		return Music1;
	}

	public static void setMusic1(JLabel music1) {
		Music1 = music1;
	}

	public static JLabel getHobbies1() {
		return Hobbies1;
	}

	public static void setHobbies1(JLabel hobbies1) {
		Hobbies1 = hobbies1;
	}

	public static boolean isSecondMsgDel() {
		return secondMsgDel;
	}

	public static boolean isFirstMsgDel() {
		return firstMsgDel;
	}

	public static boolean isChangeMembertype() {
		return changeMembertype;
	}

}
