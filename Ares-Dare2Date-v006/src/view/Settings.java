package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import model.ReadTxt4Login;

public class Settings extends JPanel {

	private static String userFirstname = "blank";
	private static String userSurname = "blank";
	private static String addressH = "blank";
	private static String creditCardNumberH = "blank";
	private static String passwordH = "blank";
	private static String blankID = "blank";
	private static String blankDOB = "blank";
	private static String blankSP = "blank";
	private static String blankMT = "blank";
	// ReadTXT4inbox.getNumberOfMessagesX();
	private static String blankMusic = "blank";
	private static String blankHobbies = "blank";
	private static String blankMovies = "blank";
	private static String blankGender = "blank";
	private static String blankSports = "blank";
	// inbox variables
	public static boolean secondMsgDel = false;
	public static boolean firstMsgDel = false;
	public static boolean thirdMsgDel;
	public static boolean fourthMsgDel;
	public static boolean fifthMsgDel;
	private static JLabel label_6;

	private static JLabel lblCrnumber;
	private static JLabel lblBlanksp;
	private static JLabel lblBlankhobbies;
	private static JLabel lblBlankmusic;
	private static JLabel lblBlanksports;
	private static JLabel lblBlankmovies;
	private static JLabel lblPasswordll;
	private static JLabel lblBlankmt;
	static JLabel lblAdres;

	public Settings() {

		JLabel label = new JLabel("UserID:");
		label.setForeground(new Color(128, 0, 0));
		label.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		label.setBounds(36, 52, 98, 14);
		add(label);

		JLabel label_1 = new JLabel("First name:");
		label_1.setForeground(new Color(128, 0, 0));
		label_1.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		label_1.setBounds(36, 74, 98, 14);
		add(label_1);

		JLabel label_2 = new JLabel("Last name:");
		label_2.setForeground(new Color(128, 0, 0));
		label_2.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		label_2.setBounds(36, 99, 98, 14);
		add(label_2);
		/*
		 * JLabel label_3 = new JLabel("Address:"); label_3.setForeground(new
		 * Color(128, 0, 0)); label_3.setFont(new Font("Yu Gothic Light",
		 * Font.BOLD, 12)); label_3.setBounds(36, 124, 98, 14); add(label_3);
		 */
		JLabel label_4 = new JLabel("Creditcard:");
		label_4.setForeground(new Color(128, 0, 0));
		label_4.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		label_4.setBounds(265, 52, 77, 14);
		add(label_4);
		/*
		 * JLabel label_5 = new JLabel("Number:"); label_5.setForeground(new
		 * Color(128, 0, 0)); label_5.setFont(new Font("Yu Gothic Light",
		 * Font.BOLD, 12)); label_5.setBounds(265, 99, 59, 14); add(label_5);
		 */
		label_6 = new JLabel("City:");
		label_6.setForeground(new Color(128, 0, 0));
		label_6.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		// label_6.setBounds(265, 124, 47, 14);
		label_6.setBounds(36, 124, 47, 14);
		add(label_6);

		/*
		 * JLabel label_7 = new JLabel("Zip code:"); label_7.setForeground(new
		 * Color(128, 0, 0)); label_7.setFont(new Font("Yu Gothic Light",
		 * Font.BOLD, 12)); label_7.setBounds(36, 149, 98, 14); add(label_7);
		 * 
		 * JLabel label_8 = new JLabel("Country:"); label_8.setForeground(new
		 * Color(128, 0, 0)); label_8.setFont(new Font("Yu Gothic Light",
		 * Font.BOLD, 12)); label_8.setBounds(265, 74, 59, 14); add(label_8);
		 */
		JLabel label_9 = new JLabel("Gender:");
		label_9.setForeground(new Color(128, 0, 0));
		label_9.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		label_9.setBounds(36, 174, 98, 14);
		add(label_9);

		JLabel label_10 = new JLabel("Membership:");
		label_10.setForeground(new Color(128, 0, 0));
		label_10.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		label_10.setBounds(36, 197, 98, 14);
		add(label_10);

		JLabel label_11 = new JLabel("Password:");
		label_11.setForeground(new Color(128, 0, 0));
		label_11.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		label_11.setBounds(36, 222, 98, 14);
		add(label_11);

		JLabel label_12 = new JLabel("Sexual preference:");
		label_12.setForeground(new Color(128, 0, 0));
		label_12.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		label_12.setBounds(36, 247, 137, 14);
		add(label_12);

		JLabel label_13 = new JLabel("Movies:");
		label_13.setForeground(new Color(128, 0, 0));
		label_13.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		label_13.setBounds(36, 298, 59, 14);
		add(label_13);

		JLabel label_14 = new JLabel("Sports:");
		label_14.setForeground(new Color(128, 0, 0));
		label_14.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		label_14.setBounds(36, 323, 59, 14);
		add(label_14);

		JLabel label_15 = new JLabel("Music:");
		label_15.setForeground(new Color(128, 0, 0));
		label_15.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		label_15.setBounds(36, 348, 47, 14);
		add(label_15);

		JLabel label_16 = new JLabel("Hobbies:");
		label_16.setForeground(new Color(128, 0, 0));
		label_16.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		label_16.setBounds(36, 373, 59, 14);
		add(label_16);

		JLabel dateofBirth = new JLabel("BirthYear:");
		dateofBirth.setForeground(new Color(128, 0, 0));
		dateofBirth.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		// dateofBirth.setBounds(265, 149, 89, 14);
		dateofBirth.setBounds(265, 74, 89, 14);
		add(dateofBirth);

		// those are the blank values
		JLabel lblBlankid = new JLabel(blankID);
		lblBlankid.setBounds(154, 52, 101, 14);
		add(lblBlankid);

		JLabel lblActualname = new JLabel(userFirstname);
		lblActualname.setBounds(154, 74, 101, 14);
		add(lblActualname);

		JLabel lblActualsname = new JLabel(userSurname);
		lblActualsname.setBounds(154, 99, 101, 14);
		add(lblActualsname);

		lblAdres = new JLabel(addressH);
		lblAdres.setBounds(154, 124, 101, 14);
		add(lblAdres);

		/*
		 * JLabel zipcode = new JLabel("blank"); zipcode.setBounds(154, 149,
		 * 101, 14); add(zipcode);
		 */

		JLabel lblBlankgender = new JLabel(blankGender);
		lblBlankgender.setBounds(154, 174, 101, 14);
		add(lblBlankgender);

		setLblPasswordll(new JLabel(passwordH));
		getLblPasswordll().setBounds(154, 222, 89, 14);
		add(getLblPasswordll());
		/*
		 * JLabel homenumber = new JLabel("blank"); homenumber.setBounds(362,
		 * 99, 77, 14); add(homenumber);
		 * 
		 * JLabel city = new JLabel("blank"); city.setBounds(362, 124, 77, 14);
		 * add(city);
		 * 
		 * JLabel Country = new JLabel("blank"); Country.setBounds(364, 74, 89,
		 * 14); add(Country);
		 */
		setLblCrnumber(new JLabel(creditCardNumberH));
		getLblCrnumber().setBounds(364, 52, 115, 14);
		add(getLblCrnumber());

		// end of the blank settings"

		// Log out button
		JLabel lblNewLabel_1f = new JLabel("New label foor picture tree");
		lblNewLabel_1f.setIcon(new ImageIcon("pic/TreeHearts.jpg"));
		// lblNewLabel_1f.setIcon(new
		// ImageIcon("C:\\Users\\Niilu\\Desktop\\love_hearts.jpg"));
		lblNewLabel_1f.setBounds(378, 162, 374, 324);
		add(lblNewLabel_1f);

		setLblBlankmt(new JLabel(blankMT));
		getLblBlankmt().setBounds(154, 197, 63, 14);
		add(getLblBlankmt());

		JLabel lblBlankdob = new JLabel(blankDOB);
		// lblBlankdob.setBounds(364, 149, 106, 14);
		lblBlankdob.setBounds(364, 74, 89, 14);
		add(lblBlankdob);

		setLblBlankmusic(new JLabel(blankMusic));
		getLblBlankmusic().setBounds(154, 348, 263, 14);
		add(getLblBlankmusic());

		setLblBlankhobbies(new JLabel(blankHobbies));
		getLblBlankhobbies().setBounds(154, 373, 264, 14);
		add(getLblBlankhobbies());

		 lblBlankmovies = new JLabel(blankMovies);
		lblBlankmovies.setBounds(154, 298, 263, 14);
		add(lblBlankmovies);

		setLblBlanksports(new JLabel(blankSports));
		getLblBlanksports().setBounds(154, 323, 263, 14);
		add(getLblBlanksports());

		setLblBlanksp(new JLabel(blankSP));
		getLblBlanksp().setBounds(154, 247, 59, 14);
		add(getLblBlanksp());

	}

	public static String getUserFirstname() {
		return userFirstname;
	}

	public static void setUserFirstname(String userFirstname) {
		Settings.userFirstname = userFirstname;
	}

	public static String getUserSurname() {
		return userSurname;
	}

	public static void setUserSurname(String userSurname) {
		Settings.userSurname = userSurname;
	}

	public static String getAddressH() {
		return addressH;
	}

	public static void setAddressH(String addressH) {
		Settings.addressH = addressH;
	}

	public static String getCreditCardNumberH() {
		return creditCardNumberH;
	}

	public static void setCreditCardNumberH(String creditCardNumberH) {
		Settings.creditCardNumberH = creditCardNumberH;
	}

	public static String getPasswordH() {
		return passwordH;
	}

	public static void setPasswordH(String passwordH) {
		Settings.passwordH = passwordH;
	}

	public static String getBlankID() {
		return blankID;
	}

	public static void setBlankID(String blankID) {
		Settings.blankID = blankID;
	}

	public static String getBlankDOB() {
		return blankDOB;
	}

	public static void setBlankDOB(String blankDOB) {
		Settings.blankDOB = blankDOB;
	}

	public static String getBlankSP() {
		return blankSP;
	}

	public static void setBlankSP(String blankSP) {
		Settings.blankSP = blankSP;
	}

	public static String getBlankMT() {
		return blankMT;
	}

	public static void setBlankMT(String blankMT) {
		Settings.blankMT = blankMT;
	}

	public static String getBlankMusic() {
		return blankMusic;
	}

	public static void setBlankMusic(String blankMusic) {
		Settings.blankMusic = blankMusic;
	}

	public static String getBlankHobbies() {
		return blankHobbies;
	}

	public static void setBlankHobbies(String blankHobbies) {
		Settings.blankHobbies = blankHobbies;
	}

	public static String getBlankMovies() {
		return blankMovies;
	}

	public static void setBlankMovies(String blankMovies) {
		Settings.blankMovies = blankMovies;
	}

	public static String getBlankGender() {
		return blankGender;
	}

	public static void setBlankGender(String blankGender) {
		Settings.blankGender = blankGender;
	}

	public static String getBlankSports() {
		return blankSports;
	}

	public static void setBlankSports(String blankSports) {
		Settings.blankSports = blankSports;
	}

	public static boolean isSecondMsgDel() {
		return secondMsgDel;
	}

	public static void setSecondMsgDel(boolean secondMsgDel) {
		Settings.secondMsgDel = secondMsgDel;
	}

	public static boolean isFirstMsgDel() {
		return firstMsgDel;
	}

	public static void setFirstMsgDel(boolean firstMsgDel) {
		Settings.firstMsgDel = firstMsgDel;
	}

	public static boolean isThirdMsgDel() {
		return thirdMsgDel;
	}

	public static void setThirdMsgDel(boolean thirdMsgDel) {
		Settings.thirdMsgDel = thirdMsgDel;
	}

	public static boolean isFourthMsgDel() {
		return fourthMsgDel;
	}

	public static void setFourthMsgDel(boolean fourthMsgDel) {
		Settings.fourthMsgDel = fourthMsgDel;
	}

	public static boolean isFifthMsgDel() {
		return fifthMsgDel;
	}

	public static void setFifthMsgDel(boolean fifthMsgDel) {
		Settings.fifthMsgDel = fifthMsgDel;
	}

	public static JLabel getLabel_6() {
		return label_6;
	}

	public static void setLabel_6(JLabel label_6) {
		Settings.label_6 = label_6;
	}

	public static JLabel getLblBlankmt() {
		return lblBlankmt;
	}

	public static void setLblBlankmt(JLabel lblBlankmt) {
		Settings.lblBlankmt = lblBlankmt;
	}

	public static JLabel getLblPasswordll() {
		return lblPasswordll;
	}

	public static void setLblPasswordll(JLabel lblPasswordll) {
		Settings.lblPasswordll = lblPasswordll;
	}

	public static JLabel getLblBlankmovies() {
		return lblBlankmovies;
	}

	public static void setLblBlankmovies(JLabel lblBlankmovies) {
		Settings.lblBlankmovies = lblBlankmovies;
	}

	public static JLabel getLblBlanksports() {
		return lblBlanksports;
	}

	public static void setLblBlanksports(JLabel lblBlanksports) {
		Settings.lblBlanksports = lblBlanksports;
	}

	public static JLabel getLblBlankmusic() {
		return lblBlankmusic;
	}

	public static void setLblBlankmusic(JLabel lblBlankmusic) {
		Settings.lblBlankmusic = lblBlankmusic;
	}

	public static JLabel getLblBlankhobbies() {
		return lblBlankhobbies;
	}

	public static void setLblBlankhobbies(JLabel lblBlankhobbies) {
		Settings.lblBlankhobbies = lblBlankhobbies;
	}

	public static JLabel getLblBlanksp() {
		return lblBlanksp;
	}

	public static void setLblBlanksp(JLabel lblBlanksp) {
		Settings.lblBlanksp = lblBlanksp;
	}

	public static JLabel getLblCrnumber() {
		return lblCrnumber;
	}

	public static void setLblCrnumber(JLabel lblCrnumber) {
		Settings.lblCrnumber = lblCrnumber;
	}
}