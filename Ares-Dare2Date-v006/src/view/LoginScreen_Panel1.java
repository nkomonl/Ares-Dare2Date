package view;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;

import model.ReadTxt4Login;
import model.ReadTxt4Search;
import util.ArrayList_Legion;
import util.PanelManager;
import util.timer;
import controller.MainFrame;

public class LoginScreen_Panel1 extends JPanel {
	 private static JTextField txtUsername;
	 private static JPasswordField passwordField;
	static String namex;
	 static String passwd;
	char[] pwdx;
	
	private static boolean guestLogin;
	

	public LoginScreen_Panel1(final PanelManager panelManager) {
		System.out.println("\n::::::::::::::::::::::::::::::Now in  LoginScreen_Panel1::::::::::::::::::::::::::\n");
		
		
		setGuestLogin(false);
		setLayout(null);
		setBackground(Color.WHITE);
		setBounds(350, 50, 800, 700);
		JLabel lblNewLabel = new JLabel("Headerlogo");
		lblNewLabel.setIcon(new ImageIcon("pic/loginscreenlogo.png"));
		lblNewLabel.setBounds(0, 0, 784, 92);
	add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Picture with hearts!");
		lblNewLabel_1.setIcon(new ImageIcon("pic/TreeHearts.jpg"));
		lblNewLabel_1.setBounds(57, 161, 348, 291);
		add(lblNewLabel_1);
		
		setTxtUsername(new JTextField());
		getTxtUsername().setBounds(525, 275, 202, 19);
		add(getTxtUsername());   
		getTxtUsername().setColumns(10);
		
		setPasswordField(new JPasswordField());
		getPasswordField().setBounds(525, 310, 202, 19);
		add(getPasswordField());
	
		
		JLabel label = new JLabel("Password:");
		label.setForeground(new Color(139, 0, 0));
		label.setFont(new Font("Tahoma", Font.BOLD, 14));
		label.setBounds(426, 310, 88, 14);
		add(label);
		
		JLabel lblUsername = new JLabel("Username:");
		lblUsername.setForeground(new Color(139, 0, 0));
		lblUsername.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblUsername.setBounds(426, 275, 88, 14);
		add(lblUsername);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.setForeground(new Color(139, 0, 0));
		btnLogin.setBounds(525, 347, 121, 20);
		add(btnLogin);
		btnLogin.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ReadTxt4Search.setGuest(false);
				namex = getTxtUsername().getText();
				 pwdx = getPasswordField().getPassword();
				passwd = String.valueOf(pwdx);
				if ((getPasswordField().getPassword().equals("")) || (getTxtUsername().getText().equals(""))) {
					System.out.print("Username or password can not be empty\n\n");
				
					JOptionPane.showMessageDialog(null, "Username or Password can not be empty", "Error",
							JOptionPane.INFORMATION_MESSAGE);
				}
				if ((!getPasswordField().getPassword().equals("")) && (!getTxtUsername().getText().equals(""))) {
					
					System.out.print("Username is.." + namex + "..& ..password is.." + passwd);
					ReadTxt4Login.checkLoginDetails();
					if (ReadTxt4Login.isLoggedin() == true) {
						Settings.setUserFirstname(ReadTxt4Login.getLine2());
						Settings.setUserSurname(ReadTxt4Login.getLine3());
						Settings.setPasswordH(ReadTxt4Login.getLine4());
						Settings.setAddressH(ReadTxt4Login.getLine5());
						Settings.setCreditCardNumberH(ReadTxt4Login.getLine6());
						Settings.setBlankGender(ReadTxt4Login.getLine7());
						Settings.setBlankDOB(ReadTxt4Login.getLine8());
						Settings.setBlankMT(ReadTxt4Login.getLine9());
						Settings.setBlankSP(ReadTxt4Login.getLine10());
						Settings.setBlankMusic(ReadTxt4Login.getLine11());
						Settings.setBlankMovies(ReadTxt4Login.getLine12());
						Settings.setBlankSports(ReadTxt4Login.getLine13());
						Settings.setBlankHobbies(ReadTxt4Login.getLine14());
						Settings.setBlankID(ReadTxt4Login.getLine1());
						
						
						ChangeSettings.setUserFirstname(ReadTxt4Login.getLine2());
						ChangeSettings.setUserSurname(ReadTxt4Login.getLine3());
						ChangeSettings.setPasswordH(ReadTxt4Login.getLine4());
						ChangeSettings.setAddressH(ReadTxt4Login.getLine5());
						ChangeSettings.setCreditCardNumberH(ReadTxt4Login.getLine6());
						ChangeSettings.setBlankGender(ReadTxt4Login.getLine7());
						ChangeSettings.setBlankDOB(ReadTxt4Login.getLine8());
						ChangeSettings.setBlankMT(ReadTxt4Login.getLine9());
						ChangeSettings.setBlankSP(ReadTxt4Login.getLine10());
						ChangeSettings.setBlankMusic(ReadTxt4Login.getLine11());
						ChangeSettings.setBlankMovies(ReadTxt4Login.getLine12());
						ChangeSettings.setBlankSports(ReadTxt4Login.getLine13());
						ChangeSettings.setBlankHobbies(ReadTxt4Login.getLine14());
						ChangeSettings.setBlankID(ReadTxt4Login.getLine1());
						
						
						
						
						
						
						
						
						timer.Counter();
						ReadTxt4Search.setGuest(true);
						
					//
						//ReadTXT4inbox.ReadInbox();
					//	Settings.setNumberOfMessages(ReadTXT4inbox.getNumberOfMessagesX());
						
						
						ArrayList_Legion.SPYotherUsers();
												MainFrame.setScreenNumber(6);
						panelManager.panelManagerI();
						
					}else{
								namex = "";
						 pwdx = null;
						passwd = "";
						
						JOptionPane.showMessageDialog(null, "Username or Password FAIL", "Login FAIL",
								JOptionPane.INFORMATION_MESSAGE);
						System.out.print("clearrrrr..................\n\n");
					}
					

				}

			}

		});
				
		JButton btnRegister = new JButton("Register!");
		btnRegister.setForeground(new Color(139, 0, 0));
		btnRegister.setBounds(576, 370, 121, 20);
		add(btnRegister);
		btnRegister.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				panelManager.panelManagerI();
			}

		});
		
		JLabel lblLoginWithYour = new JLabel("Login with your username and password.");
		lblLoginWithYour.setForeground(new Color(165, 42, 42));
		lblLoginWithYour.setFont(new Font("Tahoma", Font.ITALIC, 12));
		lblLoginWithYour.setBounds(452, 438, 348, 14);
		add(lblLoginWithYour);
		
		JLabel lblNewLabel_2 = new JLabel("If your new to this amazingness, hit that register button.");
		lblNewLabel_2.setForeground(new Color(165, 42, 42));
		lblNewLabel_2.setFont(new Font("Tahoma", Font.ITALIC, 12));
		lblNewLabel_2.setBounds(436, 462, 348, 14);
		add(lblNewLabel_2);
		
		JButton btnGuest = new JButton("Guest");
		btnGuest.setToolTipText("See people without registering.");
		btnGuest.setForeground(new Color(139, 0, 0));
		btnGuest.setBounds(623, 393, 121, 20);
		add(btnGuest);
		btnGuest.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
			
				System.out.println("Guest clicked........................");
				ReadTxt4Search.setGuest(true);
				MainFrame.setScreenNumber(14);
				panelManager.panelManagerI();				
				
			}

		});
		
	
	}
	public static JTextField getTxtUsername() {
		return txtUsername;
	}

	public static void setTxtUsername(JTextField txtUsername) {
		LoginScreen_Panel1.txtUsername = txtUsername;
	}

	public static JPasswordField getPasswordField() {
		return passwordField;
	}

	public static void setPasswordField(JPasswordField passwordField) {
		LoginScreen_Panel1.passwordField = passwordField;
	}
	public static boolean isGuestLogin() {
		return guestLogin;
	}
	public static void setGuestLogin(boolean guestLogin) {
		LoginScreen_Panel1.guestLogin = guestLogin;
	}	
}
