package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import controller.MainFrame;
import util.FileUtilDeleter;
import util.FileUtilReplacer;
import model.ReadTXT4inbox;
import model.ReadTxt4Login;

public class Inbox extends JPanel {
	private static boolean secondMsgDel = false;
	private static boolean firstMsgDel = false;
	private String messageMain;
	private String message1;
	private String Newlabel;
	private String Newlabel1;
	private String Newlabel2;
	private static String Newlabel23;
	private String Newlabel22;
	private String Newlabel3;
	private String Newlabel4;
	private String Newlabel5;
	private String Newlabel6;
	private TextArea textareaMsg;
	private String mainMessage = "Select message to Open";
	private static boolean replying = false;
	private static boolean messageHasBeenOpened;
	private static String currentSenderID;
	private static boolean thirdMsgDel;
	private static boolean fourthMsgDel;
	private static boolean fifthMsgDel;
	private static JLabel lblNewLabel_13 ;
	private static JLabel lblNewLabel_12;
	private static JLabel lblNewLabel_1;
	private static JLabel lblM;
	private static JLabel lblMessage;
	private static JLabel lblNewLabel;
	private static boolean firstroundOfdelete = false;
private static boolean m1;
private static boolean m2;
private static boolean m3;
private static boolean m4;
private static boolean m5;
	
	
	
	
	
	
	
	
	
	
	public Inbox() {
		System.out.println("********Now in inbox************ ");
		// ************************************************************read text
		// file************************************************
		ReadTXT4inbox.ReadInbox();

		/*
		 * if(ReadTXT4inbox.getLine21()!=null){
		 * message1=ReadTXT4inbox.getLine15();
		 * 
		 * }if(ReadTXT4inbox.getLine23()!=null){
		 * message1=ReadTXT4inbox.getLine15();
		 * 
		 * }if(ReadTXT4inbox.getLine25()!=null){
		 * message1=ReadTXT4inbox.getLine15();
		 * 
		 * }
		 */
		// ********************************************************put read
		// stuff to array or gui****************************************
		setLayout(null);
m1=m2=m3=m4=m5=false;
		messageHasBeenOpened = false;
		// /////////////////////////////////////////////////PANEL///////////////////////////////////////////////////////////
	/*	JPanel panel = new JPanel();
		panel.setBounds(0, 0, 800, 106);
		add(panel);
		panel.setLayout(null);

		JLabel lblDaredate = new JLabel("UserID "+ ReadTxt4Login.getUsernameEntered()+" Dare2Date personal inbox");
		lblDaredate.setFont(new Font("DialogInput", Font.BOLD, 28));
		lblDaredate.setForeground(Color.RED);
		lblDaredate.setBounds(34, 11, 689, 84);
		panel.add(lblDaredate);*/
		// /////////////////////////////////////PANEL2////////////////////////////////////////////////////////////////////////////

		JPanel panel_2 = new JPanel();
		panel_2.setBackground(new Color(139, 0, 0));
		panel_2.setBounds(309, 2, 491, 700);
		add(panel_2);
		panel_2.setLayout(null);

		// Create textarea using fifth constructor
		textareaMsg = new TextArea(mainMessage, 30, 18,
				TextArea.SCROLLBARS_VERTICAL_ONLY);
		textareaMsg.setBackground(Color.WHITE);
		textareaMsg.setBounds(31, 23, 385, 350);
		textareaMsg.setEditable(false);

		panel_2.add(textareaMsg);

		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(Color.WHITE);
		panel_3.setBounds(5, 2, 309, 456);
		add(panel_3);
		panel_3.setLayout(null);
		
		// code needed to set for a longer inbox

		lblNewLabel = new JLabel("Inbox");
		lblNewLabel.setBounds(10, 11, 120, 23);
		panel_3.add(lblNewLabel);

		 lblMessage = new JLabel(message1);
		lblMessage.setBounds(10, 55, 120, 23);
		panel_3.add(lblMessage);

		 lblM = new JLabel(Newlabel1);
		lblM.setBounds(10, 80, 201, 14);
		panel_3.add(lblM);

		lblNewLabel_1 = new JLabel(Newlabel);
		lblNewLabel_1.setBounds(10, 103, 201, 14);
		panel_3.add(lblNewLabel_1);

	 lblNewLabel_12 = new JLabel(Newlabel22);
		lblNewLabel_12.setBounds(10, 123, 201, 14);
		panel_3.add(lblNewLabel_12);

		 lblNewLabel_13 = new JLabel(Newlabel23);
		lblNewLabel_13.setBounds(10, 143, 201, 14);
		panel_3.add(lblNewLabel_13);

		
		
	if(ReadTXT4inbox.getLine23()!=null){
		lblNewLabel_13.setText(ReadTXT4inbox.getLine23());}
		if(ReadTXT4inbox.getLine21()!=null){lblNewLabel_12.setText(ReadTXT4inbox.getLine21());}
		if(ReadTXT4inbox.getLine19()!=null){lblNewLabel_1 .setText(ReadTXT4inbox.getLine19());}
		if(ReadTXT4inbox.getLine15()!=null){lblMessage .setText( ReadTXT4inbox.getLine15());}
		if(ReadTXT4inbox.getLine17()!=null){lblM .setText(ReadTXT4inbox.getLine17());}
		
		
		
		
		
		
		
		JButton btnReply = new JButton("Reply");
		btnReply.setBounds(31, 400, 385, 23);
		btnReply.setForeground(new Color(128, 0, 0));
		btnReply.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
		panel_2.add(btnReply);
		btnReply.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				System.out
						.print("\n\t\t****************REPLY CLICKED************\n ");
				if (isMessageHasBeenOpened() == true) {
					setReplying(true);
//					MainFrame.setScreenNumber(10);
//					panelManager.panelManagerI();
				}
				if (isMessageHasBeenOpened() == false) {
					System.out.println("\n***no messages selected****\n");

					JOptionPane.showMessageDialog(null,
							"Select and open a message before clicking reply",
							"Reply Error", JOptionPane.INFORMATION_MESSAGE);
				}
			}

		});

		// the above code creates same as below
		// JTextArea textAreaXX = new JTextArea(mainMessage);
		// textAreaXX.setEditable(false);
		// textAreaXX.setRows(30);
		// textAreaXX.setColumns(18);
		// textAreaXX.setBounds(29, 24, 399, 335);
		// panel_2.add(textAreaXX);
		// /////////////////////////////////////PANEL3////////////////////////////////////////////////////////////////////////////
		

		if (ReadTXT4inbox.getLine15() != null) {
			message1 = ReadTXT4inbox.getLine15();

			JButton btnDel = new JButton("Delete");
			btnDel.setBounds(205, 55, 100, 18);
			btnDel.setForeground(new Color(128, 0, 0));
			btnDel.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
			panel_3.add(btnDel);
			btnDel.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					if(m1==false){
						System.out.println("\n****delete message one fail because not yet open***\n");

						JOptionPane.showMessageDialog(null, "Open the email first in order to delete it", "Delete Fail",
								JOptionPane.INFORMATION_MESSAGE);
					}
					if(m1==true){

					setMessageHasBeenOpened(true);

					setFirstMsgDel(true);
					setSecondMsgDel(false);
					setThirdMsgDel(false);
					setFourthMsgDel(false);
					setFifthMsgDel(false);

					System.out
							.print("\n\t\t****************dMail CLICKED************\n ");
					mainMessage = ReadTXT4inbox.getLine16();
					setcurrentSenderID(ReadTXT4inbox.getLine15());

					FileUtilDeleter mod = new FileUtilDeleter(new File("Text-Files/"+ ReadTxt4Login.getUsernameEntered() + ".txt"));
					setFirstroundOfdelete(true);
					mod.deleteEntry(currentSenderID);
					setFirstroundOfdelete(false);
					mod.deleteEntry(mainMessage);

					System.out.println("*******************************");
					JOptionPane.showMessageDialog(null,
							"The chossen message has been deleted",
							"Message deleted", JOptionPane.INFORMATION_MESSAGE);
					  ReadTXT4inbox.ReadInbox();
				
					  lblNewLabel_13.setText(ReadTXT4inbox.getLine23());
						lblNewLabel_12.setText(ReadTXT4inbox.getLine21());
						lblNewLabel_1 .setText(ReadTXT4inbox.getLine19());
						lblMessage .setText( ReadTXT4inbox.getLine15());
						  lblM .setText(ReadTXT4inbox.getLine17());
						  m1=m2=m3=m4=m5=false; 
						       HomePage_Panel6.getTabbedPane().setSelectedIndex(HomePage_Panel6.getTabbedPane().getSelectedIndex() -3);
						  
						

				
				}}

			});
			JButton btnOpenMail = new JButton("OpenMail");
			btnOpenMail.setBounds(100, 55, 100, 18);
			btnOpenMail.setForeground(new Color(128, 0, 0));
			btnOpenMail.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
			panel_3.add(btnOpenMail);
			btnOpenMail.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					textareaMsg.setText("");
					setMessageHasBeenOpened(true);
					System.out
							.print("\n\t\t****************OpenMail CLICKED************\n ");
					mainMessage = ReadTXT4inbox.getLine16();
					setcurrentSenderID(ReadTXT4inbox.getLine15());
					textareaMsg.setText(mainMessage);
			m1=true;
			m2=m3=m4=m5=false;
				}

			});

		}

		if (ReadTXT4inbox.getLine17() != null) {

			setMessageHasBeenOpened(true);
			Newlabel1 = ReadTXT4inbox.getLine17();

			JButton btndMail2 = new JButton("Delete");
			btndMail2.setBounds(205, 75, 100, 18);
			btndMail2.setForeground(new Color(128, 0, 0));
			btndMail2.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
			panel_3.add(btndMail2);
			btndMail2.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					if(m2==false){System.out.println("\n********delete fail due to message in 2 not open******\n");
					

					JOptionPane.showMessageDialog(null, "Open the email first in order to delete it", "Delete Fail",
							JOptionPane.INFORMATION_MESSAGE);
					}
					if(m2==true){		System.out
							.print("\n\t\t****************deleteMail2 CLICKED************\n ");

					setMessageHasBeenOpened(true);

					setFirstMsgDel(false);
					setSecondMsgDel(true);
					setThirdMsgDel(false);
					setFourthMsgDel(false);
					setFifthMsgDel(false);

					mainMessage = ReadTXT4inbox.getLine18();
					setcurrentSenderID(ReadTXT4inbox.getLine17());

					//
					FileUtilDeleter mod = new FileUtilDeleter(new File("Text-Files/"+ ReadTxt4Login.getUsernameEntered() + ".txt"));
					setFirstroundOfdelete(true);
					mod.deleteEntry(currentSenderID);
					setFirstroundOfdelete(false);
					mod.deleteEntry(mainMessage);

					System.out.println("*******************************");
					//

					JOptionPane.showMessageDialog(null,
							"The chossen message has been deleted",
							"Message deleted", JOptionPane.INFORMATION_MESSAGE);
//					MainFrame.setScreenNumber(4);
//					panelManager.panelManagerI();
					  ReadTXT4inbox.ReadInbox();
					  lblM .setText(ReadTXT4inbox.getLine17());
					  lblNewLabel_13.setText(ReadTXT4inbox.getLine23());
						lblNewLabel_12.setText(ReadTXT4inbox.getLine21());
						lblNewLabel_1 .setText(ReadTXT4inbox.getLine19());
						lblMessage .setText( ReadTXT4inbox.getLine15());
						m1=m2=m3=m4=m5=false;
						HomePage_Panel6.getTabbedPane().setSelectedIndex(HomePage_Panel6.getTabbedPane().getSelectedIndex() -3);
						 
					}
				}

			});
			JButton btnOpenMail2 = new JButton("OpenMail");
			btnOpenMail2.setBounds(100, 75, 100, 18);
			btnOpenMail2.setForeground(new Color(128, 0, 0));
			btnOpenMail2.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
			panel_3.add(btnOpenMail2);
			btnOpenMail2.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					System.out
							.print("\n\t\t****************OpenMail2 CLICKED************\n ");
					mainMessage = ReadTXT4inbox.getLine18();
					setcurrentSenderID(ReadTXT4inbox.getLine17());
					textareaMsg.setText(mainMessage);
			m2=true;
			m1=m3=m4=m5=false;
				}

			});

		}
		if (ReadTXT4inbox.getLine19() != null) {
			setMessageHasBeenOpened(true);
			Newlabel = ReadTXT4inbox.getLine19();

			JButton btndMail3 = new JButton("Delete");
			btndMail3.setBounds(205, 95, 100, 18);
			btndMail3.setForeground(new Color(128, 0, 0));
			btndMail3.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
			panel_3.add(btndMail3);
			btndMail3.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					if(m3==false){
					System.out.println("\n*****failed to delete because of message 3 not open********\n");	
					

					JOptionPane.showMessageDialog(null, "Open the email first in order to delete it", "Delete Fail",
							JOptionPane.INFORMATION_MESSAGE);
					}
					
					if(m3==true){
					System.out
							.print("\n\t\t****************deleteMail3 CLICKED************\n ");
//					mainMessage = ReadTXT4inbox.getLine20();
//					setcurrentSenderID(ReadTXT4inbox.getLine19());
//					textareaMsg.setText(mainMessage);
//					
					
					
					
					
					
					setFirstMsgDel(false);
					setSecondMsgDel(false);
					setThirdMsgDel(true);
					setFourthMsgDel(false);
					setFifthMsgDel(false);

					mainMessage = ReadTXT4inbox.getLine20();
					setcurrentSenderID(ReadTXT4inbox.getLine19());

					//
					FileUtilDeleter mod = new FileUtilDeleter(new File("Text-Files/"+ ReadTxt4Login.getUsernameEntered() + ".txt"));
					setFirstroundOfdelete(true);
					mod.deleteEntry(currentSenderID);
					setFirstroundOfdelete(false);
					mod.deleteEntry(mainMessage);

					System.out.println("*******************************");
					//

					JOptionPane.showMessageDialog(null,
							"The chossen message has been deleted",
							"Message deleted", JOptionPane.INFORMATION_MESSAGE);
//					MainFrame.setScreenNumber(4);
//					panelManager.panelManagerI();
					ReadTXT4inbox.ReadInbox();
					lblNewLabel_13.setText(ReadTXT4inbox.getLine23());
					lblNewLabel_12.setText(ReadTXT4inbox.getLine21());
					lblNewLabel_1 .setText(ReadTXT4inbox.getLine19());
					lblMessage .setText( ReadTXT4inbox.getLine15());
					  lblM .setText(ReadTXT4inbox.getLine17());
m1=m2=m3=m4=m5=false;
					  HomePage_Panel6.getTabbedPane().setSelectedIndex(HomePage_Panel6.getTabbedPane().getSelectedIndex() -3);
					
					  
					}
				}

			});

			JButton btnOpenMail3 = new JButton("Open Mail");
			btnOpenMail3.setBounds(100, 95, 100, 18);
			btnOpenMail3.setForeground(new Color(128, 0, 0));
			btnOpenMail3.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
			panel_3.add(btnOpenMail3);
			btnOpenMail3.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					System.out
							.print("\n\t\t****************OpenMail3 CLICKED************\n ");
					mainMessage = ReadTXT4inbox.getLine20();
					setcurrentSenderID(ReadTXT4inbox.getLine19());
					textareaMsg.setText(mainMessage);
				m3=true;
				m1=m2=m4=m5=false;
				
				}

			});

		}
		if (ReadTXT4inbox.getLine21() != null) {
			setMessageHasBeenOpened(true);
			Newlabel22 = ReadTXT4inbox.getLine21();

			JButton btndMail4 = new JButton("Delete");
			btndMail4.setBounds(205, 115, 100, 18);
			btndMail4.setForeground(new Color(128, 0, 0));
			btndMail4.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
			panel_3.add(btndMail4);
			btndMail4.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					
					if(m4==false){
						System.out.println(" \n*****failed to delete dueto not open mail****\n");

						JOptionPane.showMessageDialog(null, "Open the email first in order to delete it", "Delete Fail",
								JOptionPane.INFORMATION_MESSAGE);
					}
					if(m4==true){System.out
							.print("\n\t\t****************deleteMail4 CLICKED************\n ");
//					mainMessage = ReadTXT4inbox.getLine22();
//					setcurrentSenderID(ReadTXT4inbox.getLine21());
//					textareaMsg.setText(mainMessage);
					
					setFirstMsgDel(false);
					setSecondMsgDel(false);
					setThirdMsgDel(false);
					setFourthMsgDel(true);
					setFifthMsgDel(false);

					mainMessage = ReadTXT4inbox.getLine22();
					setcurrentSenderID(ReadTXT4inbox.getLine21());

					//
					FileUtilDeleter mod = new FileUtilDeleter(new File("Text-Files/"+ ReadTxt4Login.getUsernameEntered() + ".txt"));
					setFirstroundOfdelete(true);
					mod.deleteEntry(currentSenderID);
					setFirstroundOfdelete(false);
					mod.deleteEntry(mainMessage);

					System.out.println("*******************************");
					//

					JOptionPane.showMessageDialog(null,
							"The chossen message has been deleted",
							"Message deleted", JOptionPane.INFORMATION_MESSAGE);
//					MainFrame.setScreenNumber(4);
//					panelManager.panelManagerI();
					ReadTXT4inbox.ReadInbox();
					lblNewLabel_13.setText(ReadTXT4inbox.getLine23());
					lblNewLabel_12.setText(ReadTXT4inbox.getLine21());
					lblNewLabel_1 .setText(ReadTXT4inbox.getLine19());
					lblMessage .setText( ReadTXT4inbox.getLine15());
					  lblM .setText(ReadTXT4inbox.getLine17());
					  m1=m2=m3=m4=m5=false;
					  HomePage_Panel6.getTabbedPane().setSelectedIndex(HomePage_Panel6.getTabbedPane().getSelectedIndex() -3);
					
					
					
					}
				}

			});

			JButton btnOpenMail34 = new JButton("Open Mail");
			btnOpenMail34.setBounds(100, 115, 100, 18);
			btnOpenMail34.setForeground(new Color(128, 0, 0));
			btnOpenMail34.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
			panel_3.add(btnOpenMail34);
			btnOpenMail34.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					System.out
							.print("\n\t\t****************OpenMail4 CLICKED************\n ");
					mainMessage = ReadTXT4inbox.getLine22();
					setcurrentSenderID(ReadTXT4inbox.getLine21());
					textareaMsg.setText(mainMessage);
				m4=true;
					m1=m2=m3=m5=false;
				
				}

			});

		}
		if (ReadTXT4inbox.getLine23() != null) {
			setMessageHasBeenOpened(true);
			Newlabel23 = ReadTXT4inbox.getLine23();

			JButton btndMail35 = new JButton("Delete");
			btndMail35.setBounds(205, 135, 100, 18);
			btndMail35.setForeground(new Color(128, 0, 0));
			btndMail35.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
			panel_3.add(btndMail35);
			btndMail35.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					if(m5==false){System.out
						.print("\n\t\t****************delete fail due to mail not open ************\n ");
					
					
					JOptionPane.showMessageDialog(null, "Open the email first in order to delete it", "Delete Fail",
							JOptionPane.INFORMATION_MESSAGE);

					
					
					
					
					}
					if(m5==true){System.out
							.print("\n\t\t****************deleteMail5 CLICKED************\n ");
//					mainMessage = ReadTXT4inbox.getLine24();
//					setcurrentSenderID(ReadTXT4inbox.getLine23());
//					textareaMsg.setText(mainMessage);
					
					setFirstMsgDel(false);
					setSecondMsgDel(false);
					setThirdMsgDel(false);
					setFourthMsgDel(false);
					setFifthMsgDel(true);

					mainMessage = ReadTXT4inbox.getLine24();
					setcurrentSenderID(ReadTXT4inbox.getLine23());

					//
					FileUtilDeleter mod = new FileUtilDeleter(new File("Text-Files/"+ ReadTxt4Login.getUsernameEntered() + ".txt"));
					setFirstroundOfdelete(true);
					mod.deleteEntry(currentSenderID);
					setFirstroundOfdelete(false);
					mod.deleteEntry(mainMessage);

					System.out.println("*******************************");
					//

					JOptionPane.showMessageDialog(null,
							"The chossen message has been deleted",
							"Message deleted", JOptionPane.INFORMATION_MESSAGE);
//					MainFrame.setScreenNumber(4);
//					panelManager.panelManagerI();
					ReadTXT4inbox.ReadInbox();
					lblNewLabel_13.setText(ReadTXT4inbox.getLine23());
					lblNewLabel_12.setText(ReadTXT4inbox.getLine21());
					lblNewLabel_1 .setText(ReadTXT4inbox.getLine19());
					lblMessage .setText( ReadTXT4inbox.getLine15());
					  lblM .setText(ReadTXT4inbox.getLine17());
					  m1=m2=m3=m4=m5=false;
					  HomePage_Panel6.getTabbedPane().setSelectedIndex(HomePage_Panel6.getTabbedPane().getSelectedIndex() -3);
					  
				//	Settings. getLblBlankmt().setText((FileUtilReplacer.getStuff()));
				}
					
					
				}

			});

			JButton btnOpenMail36 = new JButton("Open Mail");
			btnOpenMail36.setBounds(100, 135, 100, 18);
			btnOpenMail36.setForeground(new Color(128, 0, 0));
			btnOpenMail36.setFont(new Font("Yu Gothic Light", Font.BOLD, 12));
			panel_3.add(btnOpenMail36);
			btnOpenMail36.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					System.out
							.print("\n\t\t****************OpenMail3 CLICKED************\n ");
					mainMessage = ReadTXT4inbox.getLine24();
					setcurrentSenderID(ReadTXT4inbox.getLine23());
					textareaMsg.setText(mainMessage);
					m1=m2=m3=m4=false;
				m5=true;
				}

			});

		}

		// above is needed to expand inbox

		JLabel lblNewLabel_2 = new JLabel(Newlabel2);
		lblNewLabel_2.setBounds(10, 128, 176, 14);
		panel_3.add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel(Newlabel3);
		lblNewLabel_3.setBounds(10, 154, 201, 14);
		panel_3.add(lblNewLabel_3);

		JLabel lblNewLabel_4 = new JLabel(Newlabel4);
		lblNewLabel_4.setBounds(10, 179, 201, 14);
		panel_3.add(lblNewLabel_4);

		JLabel lblNewLabel_5 = new JLabel(Newlabel5);
		lblNewLabel_5.setBounds(10, 204, 201, 14);
		panel_3.add(lblNewLabel_5);

		JLabel lblNewLabel_6 = new JLabel(Newlabel6);
		lblNewLabel_6.setBounds(10, 229, 188, 14);
		panel_3.add(lblNewLabel_6);

		

	}

	

	public static boolean isReplying() {
		return replying;
	}

	public static void setReplying(boolean replying) {
		Inbox.replying = replying;
	}

	public static String getcurrentSenderID() {
		return currentSenderID;
	}

	public void setcurrentSenderID(String currentSenderID) {
		this.currentSenderID = currentSenderID;
	}

	public static boolean isMessageHasBeenOpened() {
		return messageHasBeenOpened;
	}

	public static void setMessageHasBeenOpened(boolean messageHasBeenOpened) {
		Inbox.messageHasBeenOpened = messageHasBeenOpened;
	}



	public static boolean isFirstroundOfdelete() {
		return firstroundOfdelete;
	}



	public static void setFirstroundOfdelete(boolean firstroundOfdelete) {
		Inbox.firstroundOfdelete = firstroundOfdelete;
	}



	public static boolean isFirstMsgDel() {
		return firstMsgDel;
	}



	public static void setFirstMsgDel(boolean firstMsgDel) {
		Inbox.firstMsgDel = firstMsgDel;
	}



	public static boolean isSecondMsgDel() {
		return secondMsgDel;
	}



	public static void setSecondMsgDel(boolean secondMsgDel) {
		Inbox.secondMsgDel = secondMsgDel;
	}



	public static boolean isThirdMsgDel() {
		return thirdMsgDel;
	}



	public static void setThirdMsgDel(boolean thirdMsgDel) {
		Inbox.thirdMsgDel = thirdMsgDel;
	}



	public static boolean isFourthMsgDel() {
		return fourthMsgDel;
	}



	public static void setFourthMsgDel(boolean fourthMsgDel) {
		Inbox.fourthMsgDel = fourthMsgDel;
	}



	public static boolean isFifthMsgDel() {
		return fifthMsgDel;
	}



	public static void setFifthMsgDel(boolean fifthMsgDel) {
		Inbox.fifthMsgDel = fifthMsgDel;
	}

}
