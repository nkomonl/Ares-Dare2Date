package view;

import javax.swing.JPanel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import java.awt.Color;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.JButton;

import model.ReadTXT4inbox;
import model.ReadTxt4Login;
import model.ReadTxt4Search;
import model.ReadUsersList;
import model.SendMessage;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.time.temporal.JulianFields;
import java.util.List;

import javax.swing.JTextField;
import javax.swing.JScrollBar;

import controller.MainFrame;
import controller.MainFrame;

import java.awt.SystemColor;

import javax.swing.border.LineBorder;

import util.PanelManager;

public class ViewOtherUserPreferences_Panel10 extends JPanel {
	private static JTextField txtAddReceiverVOUP;
	DefaultListModel<String> listModel1VOUP = new DefaultListModel<>();
	private JList<String> usersListVOUP;
	private static String addReceiverVOUP = "Add receiver";
	private static String messageVOUP;
	private static String receiverIDVOUP;
	private static String sendrIDVOUP = ReadTxt4Login.getLine1();
	private static String privateMessageVOUP = "Private Message";
	private static String blankSender2VOUP;
	private static JTextArea txtrPrivatemessage2VOUP;
	private static String user2CheckVOUP;
	private static String UserBeignChekedSexVOUP;
	private static String UserBeignChekedSexChoiceVOUP;
	private static String UserBeignChekedMusicChoiceVOUP;
	private static String UserBeignChekedCityVOUP;
	private static String UserBeignChekedSportChoiceVOUP;
	private static String UserBeignChekedMovieGenreVOUP;
	private static String UserBeignChekedHobbiesChoiceVOUP;
	private static JLabel lblUserSelectedVOUP;
	private static JLabel lblUserPreferencesVOUP;
	private static JLabel lblSexVOUP;
	private static JLabel lblTheUserMusicVOUP;
	private static JLabel lblTheUserHobbiesVOUP;
	private static JLabel lblTheUserMovieVOUP;
	private static JLabel lblTheUserSportVOUP;
	private static JLabel lblTheUserCityVOUP;

	public ViewOtherUserPreferences_Panel10(final PanelManager panelManager) {

		user2CheckVOUP = "****";
		UserBeignChekedSexVOUP = "****";
		UserBeignChekedSexChoiceVOUP = "****";
		UserBeignChekedMusicChoiceVOUP = "****";
		UserBeignChekedCityVOUP = "****";
		UserBeignChekedSportChoiceVOUP = "****";
		UserBeignChekedMovieGenreVOUP = "****";
		UserBeignChekedHobbiesChoiceVOUP = "*****";

		System.out
				.println("************************Now in ViewOtherUserPreferences_Panel10**********************");

		// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		setLayout(null);
		// read users list
//		if (Inbox_Panel5.isReplying() == true) {
//			setAddReceiver(Inbox_Panel5.getcurrentSenderID());
//			// sendrIDVOUP = HomePage_Panel3.getBlankID();
//			System.out.println("\n\nggggggggggggggggggggg\nSender id id "
//					+ sendrIDVOUP + "\nggggggggggggggg\n\nREPLY TO"
//					+ Inbox_Panel5.getcurrentSenderID());
//			Inbox_Panel5.setReplying(false);
//		}
		// this below asks for the ioexception or try and catch..the later gives
		// problems. so throw is what i choose.
		try {
			ReadUsersList.getUserslist();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// ///////////////////////////////////////////////////start of /// panel///////////////////////////

		JPanel panel = new JPanel();
		panel.setBounds(10, 121, 196, 376);
		add(panel);
		panel.setLayout(null);

		JLabel lblDaredate_1 = new JLabel("Dare2Date registered users ");
		lblDaredate_1.setBounds(10, 11, 176, 14);
		panel.add(lblDaredate_1);

		// add elements to model
		if (ReadUsersList.arrayListUsers.size() > 0) {
			int y;
			// if(HomePage_Panel3.isModelLoaded()==false){
			//HomePage_Panel3.setModelLoaded(true);

			System.out.println("\n\n" + "xgxgxgxgxgxgxgxgxgxgxg\n"
					+ ReadUsersList.arrayListUsers
					+ "xgxgxgxgxggxgxgxgxggxxg\n" + "\n\n");

			for (y = 0; y < ReadUsersList.arrayListUsers.size(); y++) {

				if (!ReadUsersList.arrayListUsers.get(y).equalsIgnoreCase(
						ReadTxt4Login.getLine1())) {
					//
					//

					listModel1VOUP.addElement(ReadUsersList.arrayListUsers.get(y));
					// }
				}
			}
		}
		// action listner for thescroll list
		JScrollPane listScrollPane = new JScrollPane();
		listScrollPane.setBounds(38, 36, 117, 120);
		usersListVOUP = new JList<String>(listModel1VOUP);
		usersListVOUP.setVisibleRowCount(2);
		usersListVOUP.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (!e.getValueIsAdjusting()) {
					// blank older data

					setUser2Check("");
					setUserBeignChekedSex("");
					setUserBeignChekedSexChoice("");
					setUserBeignChekedMusicChoice("");
					setUserBeignChekedCity("");
					setUserBeignChekedSportChoice("");
					setUserBeignChekedMovieGenre("");
					setUserBeignChekedHobbiesChoice("");

					// listModel1VOUP.removeAllElements();
					final List<String> selectedValuesList = usersListVOUP
							.getSelectedValuesList();
					System.out.println(selectedValuesList);
					List<String> str = usersListVOUP.getSelectedValuesList();
					String str2 = str.get(0);
					System.out.println(str2);
					getTxtAddReceiver().setText(str2);

					// SHOW CHOSSEN PROFILE DETAILS
					ReadTxt4Search.checkOtherUserDetails();
					setUser2Check(ReadTxt4Search.getLine1());
					lblUserSelectedVOUP.setText("User selected is .." + user2CheckVOUP);

					lblUserPreferencesVOUP
							.setText(user2CheckVOUP + " Preferences are:");
					setUserBeignChekedSex(ReadTxt4Search.getLine7());
					lblSexVOUP.setText("This user is a " + UserBeignChekedSexVOUP
							+ " and is interested in "
							+ UserBeignChekedSexChoiceVOUP + ".");
					setUserBeignChekedSexChoice(ReadTxt4Search.getLine10());
					lblSexVOUP.setText("This user is a " + UserBeignChekedSexVOUP
							+ " and is interested in "
							+ UserBeignChekedSexChoiceVOUP + ".");
					setUserBeignChekedMusicChoice(ReadTxt4Search.getLine11());
					lblTheUserMusicVOUP.setText("This User music choice is :"
							+ UserBeignChekedMusicChoiceVOUP + ".");
					setUserBeignChekedCity(ReadTxt4Search.getLine5());
					lblTheUserCityVOUP.setText("This User City is : "
							+ UserBeignChekedCityVOUP + ".");
					setUserBeignChekedSportChoice(ReadTxt4Search.getLine13());
					lblTheUserSportVOUP.setText("This User Sport choice is :"
							+ UserBeignChekedSportChoiceVOUP + ".");
					setUserBeignChekedMovieGenre(ReadTxt4Search.getLine12());
					lblTheUserMovieVOUP.setText("This User Movie Genre is :"
							+ UserBeignChekedMovieGenreVOUP + ".");
					setUserBeignChekedHobbiesChoice(ReadTxt4Search.getLine14());
					lblTheUserHobbiesVOUP.setText("This User Hobbies are :"
							+ UserBeignChekedHobbiesChoiceVOUP + ".");
					// ///////////////////////////////////////////////////////////////////////////////////////////////////
					System.out.println("/////////////////////////"
							+ UserBeignChekedCityVOUP + "//////");
				}
			}
		});
		listScrollPane.setViewportView(usersListVOUP);
		panel.add(listScrollPane);

		JButton btnSend = new JButton("send");
		btnSend.setBounds(629, 576, 89, 23);
		add(btnSend);
		btnSend.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
			
				
				
				
					JOptionPane.showMessageDialog(null,
							"Must FIRST REGISTER inorder to send messageVOUP ...! "
									+ getReceiverID(), "JOIN FIRST",
							JOptionPane.INFORMATION_MESSAGE);
					
					
			}
		});

		// /////////////////////////////// panel 1
		// //////////////////////////////////

		JPanel panel_1 = new JPanel();
		panel_1.setBounds(0, 0, 752, 110);
		add(panel_1);
		panel_1.setLayout(null);

		JLabel lblDaredate = new JLabel("Dare2Date Search page");
		lblDaredate.setFont(new Font("Monotype Corsiva", Font.BOLD, 28));
		lblDaredate.setForeground(Color.RED);
		lblDaredate.setBounds(116, 11, 636, 84);
		panel_1.add(lblDaredate);

		JButton btnBack = new JButton("back");
		btnBack.setBounds(10, 512, 89, 23);
		add(btnBack);
		btnBack.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.print("back clikked......");
				ReadTxt4Search.setGuest(false);
					MainFrame.setScreenNumber(3);
					panelManager.panelManagerI();
					
				
			}
		});

		JButton btnLogout = new JButton("LogOut");
		btnLogout.setBounds(137, 512, 89, 23);
		add(btnLogout);

		setTxtAddReceiver(new JTextField());
		getTxtAddReceiver().setText(getAddReceiver());
		getTxtAddReceiver().setBounds(233, 348, 319, 20);
		add(getTxtAddReceiver());
		getTxtAddReceiver().setColumns(10);

		blankSender2VOUP = ReadTxt4Login.getUsernameEntered();
		JLabel lblSenderLabel = new JLabel("SenderID..." + blankSender2VOUP);
		lblSenderLabel.setBounds(236, 385, 316, 14);
		add(lblSenderLabel);

		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.LIGHT_GRAY);
		panel_2.setBounds(237, 121, 481, 213);
		add(panel_2);
		panel_2.setLayout(null);
		// /////////////////////////////////////////////////////////////////////////////////////////////////////////////
		lblUserSelectedVOUP = new JLabel("User selected is .." + user2CheckVOUP);
		lblUserSelectedVOUP.setBounds(10, 11, 461, 14);
		panel_2.add(lblUserSelectedVOUP);

		lblUserPreferencesVOUP = new JLabel(user2CheckVOUP + " Preferences are:");
		lblUserPreferencesVOUP.setBounds(10, 36, 461, 14);
		panel_2.add(lblUserPreferencesVOUP);

		lblSexVOUP = new JLabel("The user is a " + UserBeignChekedSexVOUP
				+ " and is interested in " + UserBeignChekedSexChoiceVOUP + ".");
		lblSexVOUP.setBounds(10, 61, 461, 14);
		panel_2.add(lblSexVOUP);

		lblTheUserMusicVOUP = new JLabel("The User music choice is :"
				+ UserBeignChekedMusicChoiceVOUP);
		lblTheUserMusicVOUP.setBounds(10, 79, 461, 14);
		panel_2.add(lblTheUserMusicVOUP);

		lblTheUserHobbiesVOUP = new JLabel("The User Hobbies are :"
				+ UserBeignChekedHobbiesChoiceVOUP);
		lblTheUserHobbiesVOUP.setBounds(10, 96, 461, 14);
		panel_2.add(lblTheUserHobbiesVOUP);

		lblTheUserMovieVOUP = new JLabel("The User Movie Genre is :"
				+ UserBeignChekedMovieGenreVOUP);
		lblTheUserMovieVOUP.setBounds(10, 115, 461, 14);
		panel_2.add(lblTheUserMovieVOUP);

		lblTheUserSportVOUP = new JLabel("The User Sport choice is :"
				+ UserBeignChekedSportChoiceVOUP);
		lblTheUserSportVOUP.setBounds(10, 134, 461, 14);
		panel_2.add(lblTheUserSportVOUP);

		lblTheUserCityVOUP = new JLabel("The User City is : " + UserBeignChekedCityVOUP);
		lblTheUserCityVOUP.setBounds(10, 150, 461, 14);
		panel_2.add(lblTheUserCityVOUP);

		JLabel lblIfYouAre = new JLabel(
				"If you are interested in this User, you may send the User a private mesage below.");
		lblIfYouAre.setForeground(Color.RED);
		lblIfYouAre.setBounds(10, 188, 461, 14);
		panel_2.add(lblIfYouAre);

		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		panel_3.setBounds(234, 410, 488, 143);
		add(panel_3);
		panel_3.setLayout(null);

		// create list
		// usersListVOUP = new JList<>(listModel1VOUP);
		// usersListVOUP.setBounds(63, 270, 98, 110);
		// panel.add(new JScrollPane(usersListVOUP));
		// panel.add(usersListVOUP);
		//
		//
		// ///////////////////////////////////////////end of //
		// panel///////////////////////////////

		txtrPrivatemessage2VOUP = new JTextArea();
		txtrPrivatemessage2VOUP.setBounds(10, 11, 465, 119);
		panel_3.add(txtrPrivatemessage2VOUP);
		txtrPrivatemessage2VOUP.setBackground(SystemColor.info);
		txtrPrivatemessage2VOUP.setText("Private Message");
		txtrPrivatemessage2VOUP.setToolTipText("Private Message");
		btnLogout.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
			
				
				ReadTxt4Search.setGuest(false);
				MainFrame.setScreenNumber(3);
				panelManager.panelManagerI();
				
				
				
			}

		});

		System.out
				.print(" \n*****************now inside composemail_panel6************\n");

	}

	public static String getMessage() {
		return messageVOUP;
	}

	public static void setMessage(String messageVOUP) {
		ViewOtherUserPreferences_Panel10.messageVOUP = messageVOUP;
	}

	public static String getAddReceiver() {
		return addReceiverVOUP;
	}

	public static void setAddReceiver(String addReceiverVOUP) {
		ViewOtherUserPreferences_Panel10.addReceiverVOUP = addReceiverVOUP;
	}

	public static String getReceiverID() {
		return receiverIDVOUP;
	}

	public static void setReceiverID(String receiverIDVOUP) {
		ViewOtherUserPreferences_Panel10.receiverIDVOUP = receiverIDVOUP;

		// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	}

	public static JTextField getTxtAddReceiver() {
		return txtAddReceiverVOUP;
	}

	public void setTxtAddReceiver(JTextField txtAddReceiverVOUP) {
		this.txtAddReceiverVOUP = txtAddReceiverVOUP;
	}

	public static String getUser2Check() {
		return user2CheckVOUP;
	}

	public static void setUser2Check(String user2CheckVOUP) {
		ViewOtherUserPreferences_Panel10.user2CheckVOUP = user2CheckVOUP;
	}

	public static String getUserBeignChekedSex() {
		return UserBeignChekedSexVOUP;
	}

	public static void setUserBeignChekedSex(String userBeignChekedSex) {
		UserBeignChekedSexVOUP = userBeignChekedSex;
	}

	public static String getUserBeignChekedSexChoice() {
		return UserBeignChekedSexChoiceVOUP;
	}

	public static void setUserBeignChekedSexChoice(
			String userBeignChekedSexChoice) {
		UserBeignChekedSexChoiceVOUP = userBeignChekedSexChoice;
	}

	public static String getUserBeignChekedMusicChoice() {
		return UserBeignChekedMusicChoiceVOUP;
	}

	public static void setUserBeignChekedMusicChoice(
			String userBeignChekedMusicChoice) {
		UserBeignChekedMusicChoiceVOUP = userBeignChekedMusicChoice;
	}

	public static String getUserBeignChekedCity() {
		return UserBeignChekedCityVOUP;
	}

	public static void setUserBeignChekedCity(String userBeignChekedCity) {
		UserBeignChekedCityVOUP = userBeignChekedCity;
	}

	public static String getUserBeignChekedSportChoice() {
		return UserBeignChekedSportChoiceVOUP;
	}

	public static void setUserBeignChekedSportChoice(
			String userBeignChekedSportChoice) {
		UserBeignChekedSportChoiceVOUP = userBeignChekedSportChoice;
	}

	public static String getUserBeignChekedMovieGenre() {
		return UserBeignChekedMovieGenreVOUP;
	}

	public static void setUserBeignChekedMovieGenre(
			String userBeignChekedMovieGenre) {
		UserBeignChekedMovieGenreVOUP = userBeignChekedMovieGenre;
	}

	public static String getUserBeignChekedHobbiesChoice() {
		return UserBeignChekedHobbiesChoiceVOUP;
	}

	public static void setUserBeignChekedHobbiesChoice(
			String userBeignChekedHobbiesChoice) {
		UserBeignChekedHobbiesChoiceVOUP = userBeignChekedHobbiesChoice;
	}
}
