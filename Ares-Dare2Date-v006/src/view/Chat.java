package view;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import javax.swing.JTextField;

import util.FileUtilReplacer;
import util.VIP_Updater;
import model.ReadTxt4Login;
import model.SendMessage;
import model.WriteFile;
import model.WriteFilec;
import model.WriteSignUp;

import java.awt.Font;

public class Chat extends JPanel {
	private static File file;
	private static String strFilename = "";
	protected static String file_name = "";
	private JTextField tfReceiverID;
	private static File file3;
	  
	//private  Client2 enoch;

	public Chat()  {
		setBackground(Color.WHITE);
		setLayout(null);

		JPanel panel = new JPanel();
		panel.setBackground(new Color(128, 0, 0));
		panel.setBounds(52, 28, 660, 460);
		add(panel);
		panel.setLayout(null);

		tfReceiverID = new JTextField();
		tfReceiverID.setBounds(92, 237, 509, 23);
		panel.add(tfReceiverID);
		tfReceiverID.setColumns(10);
		
		
		JButton btnGetOnline = new JButton("Start Chat");
		btnGetOnline.setForeground(new Color(128, 0, 0));
		btnGetOnline.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnGetOnline.setBounds(129, 271, 214, 23);
		panel.add(btnGetOnline);
		btnGetOnline.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				System.out
						.println("===:===:===:=====:=====: this is button get online pressed===:===:===:=====:=====: ===:===:===:=====:=====: ");

				String textFieldValue = tfReceiverID.getText();
				file_name="Text-Files/"+textFieldValue+"c"+".txt";
				
				
				try {
					WriteFilec data = new WriteFilec(file_name, true);
					data.writeToFile(textFieldValue);
					// System.out.println("Text File Written To ");
				} catch (IOException e1) {
					System.out.print(e1.getMessage());

				}
				
				
				

				try {
					Runtime.getRuntime().exec("ServerClient/server.exe");
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}			
			}
		});
		
	
		btnGetOnline.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				System.out
						.println("===:===:===:=====:=====: this is button get offline pressed===:===:===:=====:=====: ===:===:===:=====:=====: ");

			}
		});

		JButton btnCheckOnlineUsers = new JButton("Accept incomming chat");
		btnCheckOnlineUsers.setForeground(new Color(128, 0, 0));
		btnCheckOnlineUsers.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnCheckOnlineUsers.setBounds(353, 271, 216, 23);
		panel.add(btnCheckOnlineUsers);
		btnCheckOnlineUsers.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				System.out
						.println("===:===:===:=====:=====: this is button check online users  pressed===:===:===:=====:=====: ===:===:===:=====:=====: ");
				deleteChatArlert ();
				try {
					Runtime.getRuntime().exec("ServerClient/client.exe");
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}			
				
				
			}
		});

		JLabel lblChatinvitation = new JLabel("Chat Away");
		lblChatinvitation.setForeground(new Color(255, 255, 255));
		lblChatinvitation.setBackground(Color.WHITE);
		lblChatinvitation.setFont(new Font("Yu Gothic Light", Font.BOLD, 40));
		lblChatinvitation.setBounds(246, 73, 221, 70);
		panel.add(lblChatinvitation);
		
	
		
		JLabel lblEnterTheId = new JLabel("Please enter the ID of the person you want to chat with below");
		lblEnterTheId.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblEnterTheId.setForeground(new Color(255, 255, 255));
		lblEnterTheId.setBounds(176, 154, 375, 14);
		panel.add(lblEnterTheId);
		
		

	}

	protected void writeNewUser() {
		// TODO Auto-generated method stub

	}
	
	
	public static void checkincomingCall () {
	
		
		file3 = new File("Text-Files/"+ ReadTxt4Login.getLine1()+"c" + ".txt");


		if (file3.exists()) {
		System.out.println("\n+++++++++++++++>>>>>> INCOMING CHAT REQUEST<<<<<<<<<<<<++++++\n");
		
		HomePage_Panel6. getLblIncomingChatRequest().setText("**Incoming Chat request");
		

		}
	}
	
	
	public static void deleteChatArlert () {
		HomePage_Panel6. getLblIncomingChatRequest().setText("");
		
		try{
			 
    		File file = new File("Text-Files/"+ ReadTxt4Login.getLine1()+"c" + ".txt");
 
    		if(file.delete()){
    			System.out.println("\n>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<"+file.getName() + " is deleted!>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<\n");
    		}else{
    			System.out.println("\n>>>>>>>>>>>>>>>>>.............................Delete operation is failed...............<<<<<<<<<<<<<<<<<<<\n");
    		}
 
    	}catch(Exception e){
 
    		e.printStackTrace();
 
    	}
		
		
	
		}
		
	
	
	
}
