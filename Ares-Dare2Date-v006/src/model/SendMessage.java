package model;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import controller.MainFrame;
import util.VIP_Updater;
import view.ChangeSettings;
import view.FindMylove;
import view.HomePage_Panel6;
import view.SendMessagePane;
import view.SignUpScreen_Panel2;

public class SendMessage {

	private static String strFilename = "";
	
	protected static String file_name = "";
	protected static String Usersid = "";
	private static String Msg;
	private static File file2;
	private static File file2b;
	private static boolean existingFile = false;

	private static String startFileNames;
	private static String senderName;
private static boolean emailing=false;
	

	public static void writeNewMessage() {
		
		try {

			if ((HomePage_Panel6.getTabbedPane().getSelectedIndex() == 1)||(MainFrame.getScreenNumber() == 2)) {
				System.out.print("................................1pppppp.Added new User...");

				if (SignUpScreen_Panel2.isAutoNewuserupdater() == true) {
					SignUpScreen_Panel2.setAutoNewuserupdater(false);
					System.out.print("................................2pppppp.Added new User...");

					int run;
					for (run = 0; run < VIP_Updater.getAllusersVIP().size(); run++) {
						Msg = "This is message informs you that there is a new user.["+WriteSignUp.getStrFilename()+"].";
						startFileNames = VIP_Updater.getAllusersVIP().get(run);
						senderName = WriteSignUp.getStrFilename();

						setStrFilename(startFileNames);
						Usersid = "Text-Files/"+ SendMessage.getStrFilename() + ".txt";
						file2 = new File(Usersid);

						System.out.println("auto-- updater");

						if (file2.exists()) {
							//
							PrintWriter out = new PrintWriter(
									new BufferedWriter(new FileWriter(Usersid,
											true)));
							out.println(senderName);
							out.println(Msg);
							out.close();
							System.out.print("................................pppppp.Added new User...");

							System.out.println("\nAdded to Users list\n");

						}
						if (!file2.exists()) {

							System.out
									.println("\n*********User not found**************\n");

						}

						System.out.println("autonewuser updater");
					}
				}
				if (ChangeSettings.isAutoupdater() == true) {
					ChangeSettings.setAutoupdater(false);
					int run;
					for (run = 0; run < VIP_Updater.getAllusersVIP().size(); run++) {
						Msg = "User "+ ReadTxt4Login.getLine1() +"has been updated.";
						startFileNames = VIP_Updater.getAllusersVIP().get(run);
						senderName = ReadTxt4Login.getLine1();

						setStrFilename(startFileNames);
						Usersid = "Text-Files/"+ SendMessage.getStrFilename() + ".txt";
						file2 = new File(Usersid);

						System.out.println("auto-- updater");

						if (file2.exists()) {
							//
							PrintWriter out = new PrintWriter(
									new BufferedWriter(new FileWriter(Usersid,
											true)));
							out.println(senderName);
							out.println(Msg);
							out.close();
							System.out.print("...Added new User...");

							System.out.println("\nAdded to Users list\n");

						}
						if (!file2.exists()) {

							System.out
									.println("\n*********User not found**************\n");

						}

					}
				}
			}
// sending asimple message and not for updating messages
			if (HomePage_Panel6.getTabbedPane().getSelectedIndex() == 4) {
				Msg = SendMessagePane.getMessage();
				startFileNames = SendMessagePane.getReceiverID();
				senderName =ReadTxt4Login.getLine1();

				setStrFilename(startFileNames);
				Usersid = "Text-Files/"+ getStrFilename() + ".txt";
				file2b = new File(Usersid);
			}

			
			
			if ((HomePage_Panel6.getTabbedPane().getSelectedIndex() == 2)) {
				Msg = FindMylove.getMessageVIPSearchP();
				startFileNames = FindMylove.getReceiverIDVIPSearchP();
				senderName = ReadTxt4Login.getLine1();

				setStrFilename(startFileNames);
				Usersid = "Text-Files/"+ getStrFilename() + ".txt";
				file2b = new File(Usersid);
			}

			
			if ((isEmailing()==true)||((HomePage_Panel6.getTabbedPane().getSelectedIndex() == 2))||((HomePage_Panel6.getTabbedPane().getSelectedIndex() == 4)))
			{
				
				System.out.println("\n++++++++++++++++++++++++"+Usersid+"======================");
			if (file2b.exists())
				{
				System.out.println("\n++++++++++++++++++++++++  <inside2b if checkk>  ======================");
				
				PrintWriter out = new PrintWriter(new BufferedWriter(
						new FileWriter(Usersid, true)));
				out.println(senderName);
				out.println(Msg);
				out.close();
				System.out.print("\n...send mssage by sendmessage or findmylove tab...\n");

				

			}
			if (!file2b.exists() ) {

				System.out.println("\n*********User not found**************\n");

			}
			setEmailing(false);
			// System.out.println("Text File Written To ");
		}} catch (IOException e) {
			System.out.print(e.getMessage());

		}

	}

	public static boolean isExistingFile() {
		return existingFile;
	}

	public static void setExistingFile(boolean existingFile) {
		SendMessage.existingFile = existingFile;
	}

	public static String getStrFilename() {
		return strFilename;
	}

	public static void setStrFilename(String strFilename) {
		SendMessage.strFilename = strFilename;
	}

	public static boolean isEmailing() {
		return emailing;
	}

	public static void setEmailing(boolean emailing) {
		SendMessage.emailing = emailing;
	}
}
