package model;

import java.io.IOException;
import javax.swing.JOptionPane;
import view.LoginScreen_Panel1;

public class ReadTxt4Login {

	private static String usernameEntered;
	private static char[] pwdEntered;
	private static String pwdSTR;
	private static String file_name = "";
	private static boolean loggedin = false;
	private static String line1;
	private static String line2;
	private static String line3;
	private static String line4;
	private static String line5;
	private static String line6;
	private static String line7;
	private static String line8;
	private static String line9;
	private static String line10;
	private static String line11;
	private static String line12;
	private static String line13;
	private static String line14;
	private static String line15;

	public void logUser() {

		System.out.println("Username entered is----" + getUsernameEntered()
				+ "\n\npassword is....." + pwdEntered
				+ "\n\nLogin clicked................*****");
		checkLoginDetails();

	}

	public static void checkLoginDetails() {
		System.out.println("\nlogchecker...");
		pwdEntered = LoginScreen_Panel1.getPasswordField().getPassword();
		pwdSTR = String.valueOf(pwdEntered);
		setUsernameEntered(LoginScreen_Panel1.getTxtUsername().getText());
		file_name = "Text-Files/" + ReadTxt4Login.getUsernameEntered() + ".txt";
		ReadFile file = new ReadFile(file_name);
		System.out.print(file_name + "*****" + pwdSTR);
		try {
			String[] aryLines = file.OpenFile();

			if (getUsernameEntered().equals(aryLines[0])
					&& (pwdSTR.equals(aryLines[3]))) {
				setLoggedin(true);
				System.out.println("Login success\n");
				System.out.println("Array length is..." + aryLines.length);
				int i;
				for (i = 0; i < aryLines.length; i++) {

					System.out.println(aryLines[i]);
					if (i == 0) {
						line1 = aryLines[0];
					}
					if (i == 1) {
						line2 = aryLines[1];
					}
					if (i == 2) {
						line3 = aryLines[2];
					}
					if (i == 3) {
						line4 = aryLines[3];
					}
					if (i == 4) {
						line5 = aryLines[4];
					}
					if (i == 5) {
						line6 = aryLines[5];
					}
					if (i == 6) {
						line7 = aryLines[6];
					}
					if (i == 7) {
						line8 = aryLines[7];
					}
					if (i == 8) {
						line9 = aryLines[8];
					}
					if (i == 9) {
						line10 = aryLines[9];
					}
					if (i == 10) {
						line11 = aryLines[10];
					}
					if (i == 11) {
						line12 = aryLines[11];
					}
					if (i == 12) {
						line13 = aryLines[12];
					}
					if (i == 13) {
						line14 = aryLines[13];
					}
					if (i == 14) {
						line15 = aryLines[14];
					}

				}
				System.out.print("\n***********\t***********\t******"
						+ "***\nThis here ends the text file being read");

			}
			// this next method is for only console, for developer only(not
			// users), therefor no popup alert.
			if (getUsernameEntered().equals(aryLines[0])
					&& (!pwdSTR.equals(aryLines[3]))) {

				System.out.println("password Fail****");

			}

		} catch (IOException e) {
			LoginScreen_Panel1.getTxtUsername().setText("");
			LoginScreen_Panel1.getPasswordField().setText("");

			System.out
					.println("\n*Sorry.*****\nLogin has failed"
							+ "...\nPLease Check your login details & Try to log again ! ");
			JOptionPane.showMessageDialog(null,
					"Login Fail.\nPlease check your login"
							+ " details & try to login again", "Error",
					JOptionPane.ERROR_MESSAGE);
		}

	}

	public static boolean isLoggedin() {
		return loggedin;
	}

	public static void setLoggedin(boolean loggedin) {
		ReadTxt4Login.loggedin = loggedin;
	}

	public static String getLine2() {
		return line2;
	}

	public static String getLine1() {
		return line1;
	}

	public static String getLine3() {
		return line3;
	}

	public static String getLine4() {
		return line4;
	}

	public static String getLine5() {
		return line5;
	}

	public static String getLine6() {
		return line6;
	}

	public static String getLine7() {
		return line7;
	}

	public static String getLine8() {
		return line8;
	}

	public static String getLine9() {
		return line9;
	}

	public static String getLine10() {
		return line10;
	}

	public static String getLine11() {
		return line11;
	}

	public static String getLine12() {
		return line12;
	}

	public static String getLine13() {
		return line13;
	}

	public static String getLine14() {
		return line14;
	}

	public static String getLine15() {
		return line15;
	}

	public static String getUsernameEntered() {
		return usernameEntered;
	}

	public static void setUsernameEntered(String usernameEntered) {
		ReadTxt4Login.usernameEntered = usernameEntered;
	}

}
