package model;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import view.LoginScreen_Panel1;
import view.SignUpScreen_Panel2;

public class WriteSignUp {
	public static String tfFirstName2 ;
	public static String tfLastName2 ;
	static char[] c ;
	public static String tfpwd2 ;
	public static String tcreditCard2 ;
	private static String xGender;
	private static String xYOB;
	private static String xMemberShipLevel ;
	private static String xSexualPreferences;
	private static String xMusic ;
	private static String xHobbies ;
	private static String xMovieTypes ;
	private static String xSports;
	private static String strFilename = "";
	// in windows the path to C drive can be set as C:/test.txt
	protected static String file_name = "";
	protected static String allUsers = "";
	public static String tfaddress;
	private static File file;
	private static File file2;
	
	private static boolean existingFile = false;
	private static int startFileNames = 10500;

	// strFilename = tfFirstName2;

	public static void writeNewUser() {
		try{
			setStrFilename(startFileNames + "");
		file_name ="Text-Files/"+ WriteSignUp.getStrFilename() + ".txt";
		allUsers="Text-Files/allUsers.txt";
		file = new File(file_name);
		file2 = new File(allUsers);

		tfFirstName2 = SignUpScreen_Panel2.getTfname().getText();
		tfLastName2 = SignUpScreen_Panel2.getTfsname().getText();
		 c = SignUpScreen_Panel2.getPwdField().getPassword();
		 tfpwd2 = String.valueOf(c);
		 tfaddress = SignUpScreen_Panel2.getAdrstfield().getText();
		 tcreditCard2 = SignUpScreen_Panel2.getTfcreditCard().getText();
		 xGender = SignUpScreen_Panel2.getGenderx();
		 xYOB=SignUpScreen_Panel2.textField_4().getText();
		 xMemberShipLevel = SignUpScreen_Panel2.getVIPstatus();
		 xSexualPreferences = SignUpScreen_Panel2.getGenderxLiked();
		 xMusic = SignUpScreen_Panel2.getMusic();
		 xHobbies = SignUpScreen_Panel2.getHobbies();
		 xSports= SignUpScreen_Panel2.getSports();
		 xMovieTypes = SignUpScreen_Panel2.getMovieTypes();
		 
			// in windows the path to C drive can be set as C:/test.txt
			// write to a file...each run writes a line

			if (file.exists()) {
				startFileNames++;
				System.out.println("file/user exists**********\n");
				writeNewUser();
				return;
			//	setExistingFile(true);

			}
			if (!file.exists()) {

				WriteFile data = new WriteFile(file_name, true);
				data.writeToFile(strFilename);
				data.writeToFile(tfFirstName2);
				data.writeToFile(tfLastName2);
				data.writeToFile(tfpwd2);
				data.writeToFile(tfaddress);
				data.writeToFile(tcreditCard2);
				data.writeToFile(xGender);
				data.writeToFile(xYOB);
				data.writeToFile(xMemberShipLevel);
				data.writeToFile(xSexualPreferences);
				data.writeToFile(xMusic);				
				data.writeToFile(xMovieTypes);
				data.writeToFile(xSports);
				data.writeToFile(xHobbies);
				System.out.println("Text File Written To System succesfully..\n" + "the month chossen is....\n");

			}
			if (file2.exists()) {
//				WriteFile data = new WriteFile(allUsers, true);
				//data.writeToFile(strFilename);
	
				
				//try {
				    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(allUsers, true)));
				    out.println(getStrFilename());
				    out.close();
				    System.out.print("...Added new User...");
			//	} catch (IOException e) {
				    //exception handling left as an exercise for the reader
				//}
				
				
				System.out.println("\nAdded to Users list\n");

			}
			if (!file2.exists()) {
				WriteFile data = new WriteFile(allUsers, true);

				data.writeToFile(getStrFilename());

				System.out.println("\nUsers list created\n");

			}


			// System.out.println("Text File Written To ");
		} catch (IOException e) {
			System.out.print(e.getMessage());

		}

	}

	public static boolean isExistingFile() {
		return existingFile;
	}

	public static void setExistingFile(boolean existingFile) {
		WriteSignUp.existingFile = existingFile;
	}

	public static String getStrFilename() {
		return strFilename;
	}

	public static void setStrFilename(String strFilename) {
		WriteSignUp.strFilename = strFilename;
	}
}
