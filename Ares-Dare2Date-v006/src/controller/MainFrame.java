package controller;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import util.PanelManager;
import util.timer;
import view.Guest_Panel4;
import view.HomePage_Panel6;
import view.LoginScreen_Panel1;
import view.SignUpScreen_Panel2;
import view.ViewOtherUserPreferences_Panel10;

public class MainFrame extends JFrame implements PanelManager {

	private static int screenNumber;
	private JPanel contentPane;
	private static LoginScreen_Panel1 loginScreen_Panel1;
	private static SignUpScreen_Panel2 signUpScreen_Panel2;
	private static Guest_Panel4 guest_Panel4;
	private static HomePage_Panel6 homePage_Panel6;
	private static ViewOtherUserPreferences_Panel10 viewOtherUserPreferences_Panel10;

	public MainFrame() {

		// the first panel is made with the code below.
		super("Dare2Date Group3");
		screenNumber = 1;
		setBounds(300, 10, 800, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(0, 0, 0, 0));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		loginScreen_Panel1 = new LoginScreen_Panel1(this);
		this.getContentPane().add(loginScreen_Panel1);
		this.setSize(800, 700);
		this.setVisible(true);
		
	}

	@Override
	public void panelManagerI() {
		// here we make the signup page

		// takes user gui to joining form page
		if (getScreenNumber() == 1) {
			MainFrame.setScreenNumber(2);
			this.getContentPane().removeAll();
			signUpScreen_Panel2 = new SignUpScreen_Panel2(this);
			this.getContentPane().add(signUpScreen_Panel2);

			// this.pack();
			this.setSize(800, 700);
			this.setVisible(true);

		}
		// takes user to gui to login page
		if (getScreenNumber() == 3) {
			MainFrame.setScreenNumber(1);
			this.getContentPane().removeAll();
			loginScreen_Panel1 = new LoginScreen_Panel1(this);
			this.getContentPane().add(loginScreen_Panel1);
			this.setSize(800, 700);
			this.setVisible(true);
		}
		// takes user to guest page
		if (getScreenNumber() == 4) {
			MainFrame.setScreenNumber(5);
			this.getContentPane().removeAll();
			guest_Panel4 = new Guest_Panel4(this);
			this.getContentPane().add(guest_Panel4);
			this.setSize(800, 700);
			this.setVisible(true);
		}
		// takes user to homepage
		if (getScreenNumber() == 6) {

			MainFrame.setScreenNumber(7);
			this.getContentPane().removeAll();
			homePage_Panel6 = new HomePage_Panel6(this);
			this.getContentPane().add(homePage_Panel6);
			this.setSize(800, 700);
			this.setVisible(true);
		}

		if (getScreenNumber() == 14) {
			this.setScreenNumber(15);
			this.getContentPane().removeAll();
			viewOtherUserPreferences_Panel10 = new ViewOtherUserPreferences_Panel10(this);
			this.getContentPane().add(viewOtherUserPreferences_Panel10);
			this.setSize(800, 700);
			this.setVisible(true);
		}
		
		
	}

	public static int getScreenNumber() {
		return screenNumber;
	}

	public static void setScreenNumber(int screenNumber) {
		MainFrame.screenNumber = screenNumber;
	}

}